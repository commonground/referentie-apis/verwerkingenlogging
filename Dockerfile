# Stage 1 - Compile needed python dependencies
FROM python:3.7-stretch AS build

ARG REQUIREMENTS_FILE="production.txt"
ENV REQUIREMENTS_FILE $REQUIREMENTS_FILE


RUN apt-get update && apt-get install -y --no-install-recommends \
        libpq-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY ./requirements /app/requirements
RUN pip install pip setuptools -U
RUN pip install -r requirements/${REQUIREMENTS_FILE}


# Stage 2 - build frontend
FROM mhart/alpine-node:10 AS frontend-build

WORKDIR /app

COPY ./*.json /app/
RUN npm ci

COPY ./Gulpfile.js /app/
COPY ./build /app/build/

COPY src/vlc/sass/ /app/src/vlc/sass/
RUN npm run build


# Stage 3 - Prepare gitlab test image
FROM build AS gitlab

# Stage 3.1 - Set up the needed testing/development dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /usr/local/lib/python3.7 /usr/local/lib/python3.7
COPY --from=build /app/requirements /app/requirements

RUN pip install -r requirements/ci.txt --exists-action=s

# Stage 3.2 - Set up testing config
COPY ./setup.cfg /app/setup.cfg
COPY ./bin/runtests.sh /runtests.sh

# Stage 3.3 - Copy source code
COPY --from=frontend-build /app/src/vlc/static/css /app/src/vlc/static/css
COPY ./src /app/src
ARG COMMIT_HASH
ENV GIT_SHA=${COMMIT_HASH}

RUN mkdir /app/log
CMD ["/runtests.sh"]


# Stage 4 - Prepare gitlab publish image
FROM gitlab AS publish

COPY docs/ /app/docs/


# Stage 5 - Build docker image suitable for execution and deployment
FROM python:3.7-stretch AS production

# Stage 5.1 - Set up the needed production dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /usr/local/lib/python3.7 /usr/local/lib/python3.7
COPY --from=build /usr/local/bin/uwsgi /usr/local/bin/uwsgi
# required for swagger2openapi conversion
COPY --from=frontend-build /app/node_modules /app/node_modules

# Stage 5.2 - Copy source code
WORKDIR /app
COPY ./bin/docker_start.sh /start.sh
RUN mkdir /app/log

COPY --from=frontend-build /app/src/vlc/static/css /app/src/vlc/static/css
COPY ./src /app/src
ARG COMMIT_HASH
ENV GIT_SHA=${COMMIT_HASH}

ENV DJANGO_SETTINGS_MODULE=vlc.conf.docker

ARG SECRET_KEY=dummy
ENV SECRET_KEY $SECRET_KEY

# Run collectstatic, so the result is already included in the image
RUN python src/manage.py collectstatic --noinput

EXPOSE 8000
CMD ["/start.sh"]
