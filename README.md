# Verwerkingenlogging

Referentieimplementatie voor de API-standaard voor logging van verwerkingen, zoals beschreven
op https://github.com/VNG-Realisatie/gemma-verwerkingenlogging

## Documentatie

- [Database design](https://commonground.gitlab.io/referentie-apis/verwerkingenlogging/database.html)
- [Autorisatie](https://commonground.gitlab.io/referentie-apis/verwerkingenlogging/autorisatie.html)
- [Demo](https://commonground.gitlab.io/referentie-apis/verwerkingenlogging/demo.html)
- [Postman](https://commonground.gitlab.io/referentie-apis/verwerkingenlogging/postman.html)

## Quickstart

Voordat je het project lokaal kan uitvoeren moet je je SECRET_KEY genereren, dit kan bijvoorbeeld
via https://djecrety.ir/. Dit is een willekeurige waarde van 50 karakters die gebruikt wordt voor verschillende
onderdelen (zoals het ondertekenen van de Json Web Tokens).

```sh
export SECRET_KEY="YOUR_SECRET_KEY"
```

Als laatst start je het project met docker-compose:

```sh
docker-compose up -d
```

Zie de [demo pagina](https://commonground.gitlab.io/referentie-apis/verwerkingenlogging/demo.html) in de docs voor meer
informatie.