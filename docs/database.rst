Database design
===============

Database diagram
----------------

ERD diagram van de PostgreSQL database.

.. image:: /_static/entity-relationship-diagram.png


Wijzigbaarheid en historie
--------------------------

In ontwerp B3891_ wordt er gesproken over een "fysiek onwijzigbaar" en "logisch muteerbaar" ontwerp voor de verwerkingslog.
In de referentie-implementatie zijn de records onwijzigbaar op applicatie niveau (in de database-laag) en dus in theorie fysiek wijzigbaar.
Het behoud van de historie (wat het ontwerp noemt "logisch verwijderen") is geïmplementeerd door gebruik te maken van soft-deletes en versionering.


Soft deletes
************

Elk Verwerkingsactie record heeft standaard een lege waarde voor de `tijdstipVerwijderd` kolom in de database.
Op het moment dat een record wordt verwijderd in de API, wordt deze waarde aangepast naar de huidige datum en tijd.
Hierdoor wordt er op database-niveau dus nooit een record verwijderd.


Versionering
************

Elk Verwerkingsactie-record bevat een `versie` kolom waar het versienummer van het record wordt opgeslagen en een `actieId` kolom met een UUID die alle versies met elkaar delen.
Wanneer er op applicatieniveau een wijziging plaats gaat vinden, dan:

1. Wordt het record gedupliceerd.
2. Wordt het versienummer vermeerderd met één.


.. _B3891: https://vng-realisatie.github.io/gemma-verwerkingenlogging/achtergronddocumentatie/ontwerp/artefacten/3891