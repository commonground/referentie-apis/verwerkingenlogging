Demo
====

Systeemvereisten
----------------

- Docker (https://www.docker.com/get-started)
- Docker Compose (https://docs.docker.com/compose/install/)
- Git (https://git-scm.com/)


Het project starten
-------------------

Download het project vanaf GitLab_:

.. code-block:: bash

   git clone https://gitlab.com/commonground/referentie-apis/verwerkingenlogging.git

Voordat je het project lokaal kan uitvoeren moet je je ``SECRET_KEY`` genereren, dit kan bijvoorbeeld via https://djecrety.ir/.
Dit is een willekeurige waarde van 50 karakters die gebruikt wordt voor verschillende onderdelen (zoals het ondertekenen van de Json Web Tokens).

.. code-block:: bash

   export SECRET_KEY="YOUR_SECRET_KEY"


Als laatst start je het project met docker-compose:

.. code-block:: bash

   docker-compose up -d


.. _GitLab: https://gitlab.com/commonground/referentie-apis/verwerkingenlogging


Json Web Token Genereren
------------------------

Wanneer het project draait kun je een token genereren met het volgende commando:

.. code-block:: bash

   docker-compose exec api python ./manage.py create_token read:confidential create:confidential update:confidential delete:confidential



Waar ``read:confidential create:confidential update:confidential delete:confidential`` een lijst is van scopes gescheiden door een spatie (in dit geval de scopes om een super-user te worden).


Verbinden met de PostgreSQL database
------------------------------------

Wanneer het project draait kun je met een PostgreSQL client inloggen op de database met de volgende aanmeldgegevens:

.. code-block:: yaml

   DSN:       postgres://postgres:postgres@localhost:5432/verwerkingenlogging
   Username:  postgres
   Password:  postgres
   Hostname:  localhost
   Port:      5432
   Database:  verwerkingenlogging