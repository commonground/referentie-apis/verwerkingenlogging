Autorisatie
===========


Scopes
------

Voor authenticatie wordt gebruik gemaakt van bearer authenticatie met het Json Web Token formaat.
Bij het aanmaken van een Json Web Token in de referentie-implementatie wordt gevraagd om de scopes welke worden vertaald naar permissies.
Deze scopes worden normaliter gebruikt bij authenticatie via oauth2 of OpenID Connect.


Permissies
----------

In de referentie-implementatie wordt autorisatie toegepast door middel van de permissies in de uitgegeven Json Web Tokens.
De scopes die zijn gevraagd bij het aanmaken van deze tokens worden vertaald naar een lijst met permissies.


Voorbeeld
*********

Een Json Web Token met als scope ``create:normal`` en ``delete:confidential`` resulteerd in de volgende payload:

.. code-block:: json

   {
     "token_type": "access",
     "exp": 1706712707,
     "jti": "a48a20f2e15c42a28d75f9d43d641d30",
     "permissions": [
       "create.normal",
       "delete.normal",
       "delete.restricted",
       "delete.confidential"
     ],
     "sub": 1,
     "iss": "verwerkingenlogging-referentie-implementatie"
   }