Postman
=======

Alle endpoints van de API zijn eenvoudig te testen via Postman_ met de volgende configuratie: :download:`Postman Collectie </_static/postman_collection.json>`.


.. _Postman: https://www.postman.com/