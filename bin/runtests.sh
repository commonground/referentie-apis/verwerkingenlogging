#!/bin/sh

set -e
set -x

# Wait for the database container
# See: https://docs.docker.com/compose/startup-order/
export PGHOST=${DB_HOST:-db}
export PGUSER=${DB_USER:-postgres}

until pg_isready; do
  >&2 echo "Waiting for database connection..."
  sleep 1
done

coverage run src/manage.py test vlc
coverage report --show-missing
