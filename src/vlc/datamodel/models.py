import uuid

from django.core.validators import RegexValidator
from django.db import models
from django.db.models import UniqueConstraint
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from vlc.api.permissions import PermissionType
from vlc.datamodel.revision import VerwerkingsactieVersieManager, clone


class VerwerktSoortGegeven(models.Model):
    """
    Model voor verwerktesoortgegevens
    """

    soortGegeven = models.CharField(max_length=242, unique=True)


class VerwerktObject(models.Model):
    """
    Model voor verwerkte objecten
    """

    class Objecttype(models.TextChoices):
        """
        Enum met beschikbare objecttypes
        """

        PERSOON = "persoon"

    class Meta:
        verbose_name = _("Verwerkte object")
        verbose_name_plural = _("Verwerkte objecten")
        ordering = (
            "-verwerkingsactie__tijdstip",
            "verwerktObjectId",
        )

    verwerkingsactie = models.ForeignKey(
        "Verwerkingsactie",
        related_name="verwerkteObjecten",
        on_delete=models.CASCADE,
        help_text=_(
            "Een verwerkingsactie is een operatie die wordt uitgevoerd door een "
            "geautomatiseerd systeem waarbij er (persoons)gegevens verwerkt worden. "
            "Een verwerkingsactie wordt uitgevoerd als onderdeel van (een handeling "
            "van) een verwerking. De inzage van deze verwerkingsactie is beperkt in "
            "de zin dat gevoelige gegevens zoals systeem, gebruiker en gegevensbron "
            "zijn weggelaten."
        ),
    )
    objecttype = models.CharField(
        max_length=7,
        choices=Objecttype.choices,
        help_text=_("Soort object dat verwerkt is."),
    )
    soortObjectId = models.CharField(
        max_length=242,
        help_text=_("Soort identificator waarmee het object geïdentificeerd wordt."),
    )
    objectId = models.CharField(
        max_length=40,
        null=True,
        blank=True,
        help_text=_("Identificator van het verwerkte object."),
    )
    betrokkenheid = models.CharField(
        max_length=242,
        null=True,
        blank=True,
        help_text=_(
            "Geeft aan in welke hoedanigheid de persoon of het object betrokken "
            "was bij de verwerkingsactie."
        ),
    )
    verwerkteSoortenGegevens = models.ManyToManyField(VerwerktSoortGegeven)
    verwerktObjectId = models.UUIDField(
        unique=True,
        default=uuid.uuid4,
        help_text=_(
            "Identificator van het verwerkte object. Deze ID wordt automatisch "
            "aangemaakt door het log.",
        ),
    )


class Verwerkingsactie(models.Model):
    """
    Model voor verwerkingsacties
    """

    class Meta:
        verbose_name = _("Verwerkingsactie")
        verbose_name_plural = _("Verwerkingsacties")
        ordering = ("-tijdstipRegistratie",)

        constraints = [
            UniqueConstraint(name="unique_actieId_versie", fields=["actieId", "versie"])
        ]

    class Vertrouwelijkheid(models.TextChoices):
        """
        Enum met mogelijke opties voor de vertrouwelijkheid van verwerkingsacties
        """

        NORMAAL = "normaal"
        VERTROUWELIJK = "vertrouwelijk"
        OPGEHEVEN = "opgeheven"

        @classmethod
        def get_permission(cls, permission):
            permission_mapping = {
                PermissionType.NORMAL: cls.NORMAAL,
                PermissionType.CONFIDENTIAL: cls.VERTROUWELIJK,
            }

            return permission_mapping[permission]

    objects = VerwerkingsactieVersieManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    actieId = models.UUIDField(default=uuid.uuid4, editable=False)
    versie = models.IntegerField(default=1, editable=False)

    actieNaam = models.CharField(max_length=242, null=True, blank=True)
    handelingNaam = models.CharField(max_length=242, null=True, blank=True)

    verwerkingNaam = models.CharField(max_length=242, null=True, blank=True)
    verwerkingId = models.UUIDField(null=True, blank=True)
    verwerkingsactiviteitId = models.UUIDField(
        null=True,
        blank=True,
        help_text=_("Het ID van het verwerkingsactiviteit."),
    )
    verwerkingsactiviteitUrl = models.URLField(max_length=2042, null=True, blank=True)

    verwerkingsactiviteitIdAfnemer = models.UUIDField(null=True, blank=True)
    verwerkingsactiviteitUrlAfnemer = models.URLField(
        max_length=2042, null=True, blank=True
    )
    verwerkingIdAfnemer = models.UUIDField(null=True, blank=True)

    vertrouwelijkheid = models.CharField(
        max_length=13,
        choices=Vertrouwelijkheid.choices,
        default=Vertrouwelijkheid.NORMAAL,
    )
    bewaartermijn = models.CharField(max_length=100, null=True, blank=True)
    uitvoerder = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        validators=[
            RegexValidator(
                regex="[0-9]{20}",
                message="het veld uitvoerder moet een geldig OIN bevatten.",
            )
        ],
    )
    systeem = models.CharField(max_length=242, null=True, blank=True)
    gebruiker = models.CharField(max_length=40, null=True, blank=True)
    gegevensbron = models.CharField(max_length=242, null=True, blank=True)

    soortAfnemerId = models.CharField(max_length=242, null=True, blank=True)
    afnemerId = models.CharField(max_length=40, null=True, blank=True)

    tijdstip = models.DateTimeField()
    tijdstipRegistratie = models.DateTimeField(auto_now_add=True, editable=False)
    tijdstipVerwijderd = models.DateTimeField(null=True, editable=False)

    vervallen = models.BooleanField(default=False)

    def __str__(self):
        return "{} ({})".format(self.actieNaam, self.actieId)

    def save(self, *args, **kwargs):
        if not self._state.adding:
            if self.actieId is not None:
                actieId = kwargs.get("previous_actieId", self.actieId)
                # fmt:off
                latest_version = (
                    Verwerkingsactie.objects.values_list("versie", flat=True)
                    .filter(actieId=actieId)
                    .order_by("versie")
                    .last()
                )
                # fmt:on

                with clone(self, "verwerkingsactie"):
                    self.pk = None

                    if kwargs.get("reset_version"):
                        self.versie = 1
                    elif latest_version:
                        self.versie = latest_version + 1
                    else:
                        self.versie = 1

                return

        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.tijdstipVerwijderd is None:
            self.tijdstipVerwijderd = timezone.now()
            self.vervallen = True
            self.save()

    @property
    def is_active(self):
        return not self.vervallen
