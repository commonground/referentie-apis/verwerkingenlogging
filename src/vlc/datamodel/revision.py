from contextlib import contextmanager

from django.db import models
from django.db.models.expressions import RawSQL


class VerwerkingsactieQuerySet(models.QuerySet):
    def last_versions(self):
        """
        Omdat we enkel de laatste versies van een record terug willen geven gebruiken wij een CTE
        welke de Verwerkingsacties groupeerd op basis van het 'actieId'. Op basis van deze data
        verzamelen wij alle IDs en wordt de queryset hier op gefiltert.
        """

        # TODO: use django's ORM to perform this action
        latest_records = RawSQL(
            " WITH revisions AS ("
            '   SELECT "actieId", MAX(versie) versie'
            "   FROM datamodel_verwerkingsactie "
            '   GROUP BY "actieId"'
            " )"
            " SELECT v.id"
            " FROM datamodel_verwerkingsactie v"
            ' JOIN revisions r ON r."actieId" = v."actieId"'
            "   AND r.versie = v.versie",
            (),
        )
        return self.filter(id__in=latest_records)

    def active(self):
        return self.last_versions().filter(vervallen=False)


class VerwerkingsactieVersieManager(
    models.Manager.from_queryset(VerwerkingsactieQuerySet)
):
    pass


# TODO: test this method properly
@contextmanager
def clone(instance, fk, *args, **kwargs):
    m2m = {}
    related = {}

    for related_object in instance._meta.related_objects:
        field_name = related_object.get_accessor_name()
        related_manager = getattr(instance, field_name)
        items = [item for item in related_manager.all()]

        unique_fields = [
            field
            for field in related_manager.model._meta.get_fields()
            if field.unique and not field.primary_key
        ]

        related[field_name] = {
            "instances": items,
            "unique_fields": unique_fields,
        }

    for many_to_many in instance._meta.many_to_many:
        many_related_manager = getattr(instance, many_to_many.name)
        field_name = many_to_many.m2m_field_name()
        through_model = many_related_manager.through
        filters = {"{}__pk".format(field_name): instance.pk}
        items = [item for item in through_model.objects.filter(**filters).all()]

        m2m[field_name] = items

    yield

    instance.save_base(*args, **kwargs, force_insert=True, force_update=False)

    for field_name, data in related.items():
        items = data["instances"]
        unique_fields = data["unique_fields"]

        for item in items:
            with clone(item, field_name):
                item.pk = None

                # regenerate values for unique fields if they have a default defined
                for unique_field in unique_fields:
                    if getattr(unique_field, "default", None):
                        setattr(item, unique_field.name, unique_field.default())

                setattr(item, fk, instance)

    for (field_name, items) in m2m.items():
        for item in items:
            with clone(item, field_name):
                item.pk = None
                setattr(item, field_name, instance)
