from django.contrib import admin

from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class TijdstipVerwijderdFilter(admin.SimpleListFilter):
    title = "verwijderd"
    parameter_name = "tijdstipVerwijderd"

    def lookups(self, request, model_admin):
        return (("deleted", "Verwijderd"),)

    def queryset(self, request, queryset):
        if self.value() == "deleted":
            return queryset.filter(tijdstipVerwijderd__isnull=False)

        return queryset.filter(tijdstipVerwijderd__isnull=True)

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                "selected": self.value() == str(lookup),
                "query_string": changelist.get_query_string(
                    {self.parameter_name: lookup}
                ),
                "display": title,
            }


@admin.register(Verwerkingsactie)
class VerwerkingsactieAdmin(admin.ModelAdmin):
    list_filter = (TijdstipVerwijderdFilter,)


@admin.register(VerwerktObject)
class VerwerktObjectAdmin(admin.ModelAdmin):
    list_filter = ("objecttype",)
    filter_horizontal = ("verwerkteSoortenGegevens",)

    raw_id_fields = ("verwerkingsactie",)
    readonly_fields = ("verwerktObjectId",)
