from django.conf import settings


class VersionHeaderMiddleware:
    """This class ensures the return of an API version number in every response header.
    It is called as a middleware in base.py"""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response["API-Version"] = settings.API_VERSION
        return response
