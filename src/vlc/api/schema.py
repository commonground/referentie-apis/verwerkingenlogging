from django.conf import settings

from drf_yasg import openapi

from vlc.api.permissions import Permission

description = f"""Een API om een verwerkingenlogging (VLC) te benaderen.


De VERWERKINGSACTIE is het kernobject in deze API. De Verwerkingenlogging API heeft
tot dusverre geen relaties met andere API's.

**Autorisatie**

Dee API vereist autorisatie. Er is zijn eindpoints aangemaakt om een token te
verkrijgen. Meer informatie over het aanmaken of refreshen van een token is te vinden
onder het kopje "token" in de zijbalk. Een volledige lijst met scopes is hieronder
te vinden. In de zijbalk is er meer informatie te vinden over beide endpoints.

De scopes corresponderen met de `vertrouwelijkheid`'s waarde op een VERWERKINGSACTIE.

**Scopes**

{" ".join(f"`{scope.value}`" for scope in Permission)}

**Handige links**

* [Documentatie]({settings.DOCUMENTATION_URL}/standaard)
* [Zaakgericht werken]({settings.DOCUMENTATION_URL})
* [Postman collectie]({settings.POSTMAN_COLLECTION_URL})
"""

info = openapi.Info(
    title=f"{settings.PROJECT_NAME} API",
    default_version=settings.API_VERSION,
    description=description,
    contact=openapi.Contact(
        email="standaarden.ondersteuning@vng.nl", url=settings.DOCUMENTATION_URL
    ),
    license=openapi.License(
        name="EUPL 1.2", url="https://opensource.org/licenses/EUPL-1.2"
    ),
)
