from django.conf import settings
from django.urls import include, path, re_path

from vng_api_common import routers
from vng_api_common.schema import SchemaView

from vlc.api.views import (
    TokenObtainPairView,
    TokenRefreshView,
    VerwerkingsactieViewSet,
    VerwerktObjectViewSet,
)

router = routers.DefaultRouter()
router.register("verwerkingsacties", VerwerkingsactieViewSet)
router.register("verwerkte-objecten", VerwerktObjectViewSet)

urlpatterns = [
    re_path(
        r"^v(?P<version>\d+)/",
        include(
            [
                re_path(
                    r"^schema/openapi(?P<format>\.json|\.yaml)$",
                    SchemaView.without_ui(cache_timeout=settings.SPEC_CACHE_TIMEOUT),
                    name="schema-json",
                ),
                path(
                    "schema/",
                    SchemaView.with_ui(
                        "redoc", cache_timeout=settings.SPEC_CACHE_TIMEOUT
                    ),
                    name="schema-redoc",
                ),
                path(
                    "auth/", include("rest_framework.urls", namespace="rest_framework")
                ),
                path("token/", TokenObtainPairView.as_view(), name="token-obtain-pair"),
                path(
                    "token/refresh/", TokenRefreshView.as_view(), name="token-refresh"
                ),
                path("", include(router.urls)),
                path("", include("vng_api_common.api.urls")),
            ]
        ),
    ),
]
