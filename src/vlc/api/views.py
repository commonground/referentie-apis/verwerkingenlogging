import uuid

from django.http.response import Http404
from django.utils import timezone
from django.utils.translation import gettext as _

from rest_framework import mixins, serializers, status, views, viewsets
from rest_framework.exceptions import ErrorDetail, NotFound
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import AUTH_HEADER_TYPES
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.serializers import TokenRefreshSerializer

from vlc.api.exceptions import (
    BadRequestResponse,
    InternalServerErrorResponse,
    NotFoundResponse,
)
from vlc.api.filters import (
    VerwerkingsactieFilterBackend,
    VerwerkingsactiesFilter,
    VerwerkteObjectenFilter,
)
from vlc.api.permissions import (
    Permission,
    VerwerkingsactiePermissions,
    VerwerktObjectPermissions,
)
from vlc.api.serializers import (
    TokenObtainPairSerializer,
    VerwerkingsactiePartialUpdateSerializer,
    VerwerkingsactieSerializer,
    VerwerktObjectSerializer,
)
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class TokenViewBase(views.APIView):
    """
    A custom TokenViewBase because it does not use models or querysets at all.
    The default TokenViewBase derives from GenericAPIView which for example expects
    a `queryset` attribute or `get_queryset` to be defined/implemented.
    """

    permission_classes = ()
    authentication_classes = ()
    serializer_class = None

    www_authenticate_realm = "api"

    def get_authenticate_header(self, request):
        return '{0} realm="{1}"'.format(
            AUTH_HEADER_TYPES[0],
            self.www_authenticate_realm,
        )

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs.setdefault("context", self.get_serializer_context())
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method." % self.__class__.__name__
        )

        return self.serializer_class

    def get_serializer_context(self):
        return {"request": self.request, "format": self.format_kwarg, "view": self}

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class TokenObtainPairView(TokenViewBase):
    serializer_class = TokenObtainPairSerializer


class TokenRefreshView(TokenViewBase):
    serializer_class = TokenRefreshSerializer


class VertrouwelijkheidsViewMixin:
    """
    Filters objects based on the users permissions and its corresponding vertrouwelijkheids value.
    """

    vertrouwelijkheid_lookup = None

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.request and self.request.user.is_authenticated:
            permissions = self.request.user.get_request_permissions(self.request.method)
        else:
            permissions = []

        verwerkingactie_permissions = {
            Verwerkingsactie.Vertrouwelijkheid.get_permission(permission)
            for permission in permissions
        }

        if not self.vertrouwelijkheid_lookup.endswith("in"):
            lookup = f"{self.vertrouwelijkheid_lookup}__in"
        else:
            lookup = self.vertrouwelijkheid_lookup

        return queryset.filter(
            **{
                lookup: verwerkingactie_permissions,
            },
        )

    def get_object(self):
        """
        Overrides the default `get_object` so that the endpoint returns an 403 instead of
        a 404. The default behavior is to use the queryset retrieved from `get_queryset`,
        but in our case this might not include the object we want to use and therefore returns a 404.
        """
        queryset = self.filter_queryset(self.queryset)

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs, (
            "Expected view %s to be called with a URL keyword argument "
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            "attribute on the view correctly."
            % (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {
            self.lookup_field: self.kwargs[lookup_url_kwarg],
        }
        obj = self._get_object(queryset, filter_kwargs)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def _get_object(self, queryset, filter_kwargs):
        raise NotImplemented


class VerwerkingsactieViewSet(
    VertrouwelijkheidsViewMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    lookup_field = "actieId"
    vertrouwelijkheid_lookup = "vertrouwelijkheid"
    queryset = Verwerkingsactie.objects.all()
    serializer_class = VerwerkingsactieSerializer
    filter_backends = (VerwerkingsactieFilterBackend,)
    filterset_class = VerwerkingsactiesFilter
    permission_classes = (VerwerkingsactiePermissions,)

    permissions = {
        "GET": [Permission.READ_NORMAL, Permission.READ_CONFIDENTIAL],
        "POST": [Permission.CREATE_NORMAL, Permission.CREATE_CONFIDENTIAL],
        "PATCH": [Permission.UPDATE_NORMAL, Permission.UPDATE_CONFIDENTIAL],
        "PUT": [Permission.UPDATE_NORMAL, Permission.UPDATE_CONFIDENTIAL],
        "DELETE": [Permission.DESTROY_NORMAL, Permission.DESTROY_CONFIDENTIAL],
    }

    def get_queryset(self):
        if self.action == "list":
            return super().get_queryset().active()
        return self.queryset.last_versions()

    def get_object(self):
        instance = super().get_object()

        if not instance.is_active:
            raise Http404

        return instance

    def _get_object(self, queryset, filter_kwargs):
        instances = queryset.filter(**filter_kwargs)
        instance = (
            instances.first()
        )  # most recent ordering of tijdstipRegistratie is applied here

        if not instance:
            raise Http404(
                "No %s matches the given query." % queryset.model._meta.object_name
            )
        return instance

    def get_serializer_class(self):
        if getattr(self.request, "method", None) == "PATCH":
            return VerwerkingsactiePartialUpdateSerializer

        return self.serializer_class

    def get_serializer_context(self):
        return {
            **super().get_serializer_context(),
            "permissions": self.request.user.get_request_permissions(
                self.request.method
            )
            if self.request and self.request.user.is_authenticated
            else [],
        }

    def list(self, request, *args, **kwargs):
        filters = self.filterset_class.get_filters()
        field_errors = {}

        for key in request.query_params:
            if key not in filters:
                field_errors[key] = ErrorDetail(
                    "Filter {} is niet bekend".format(key), code="unknown"
                )

            values = request.query_params.getlist(key)

            if values and len(values) > 1:
                field_errors[key] = ErrorDetail(
                    "Filter {} ondersteund geen meerdere waardes".format(key),
                    code="value",
                )

        if field_errors:
            raise serializers.ValidationError(field_errors)
        return super().list(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        verwerking_id = request.query_params.get("verwerkingId", None)

        if verwerking_id is None:
            raise serializers.ValidationError(
                {
                    "verwerkingId": ErrorDetail(
                        "Het veld verwerkingId is verplicht.", code="required"
                    ),
                }
            )

        try:
            uuid.UUID(verwerking_id)
        except ValueError:
            raise serializers.ValidationError(
                {
                    "verwerkingId": ErrorDetail(
                        "Het veld verwerkingId moet een UUID bevatten.", code="invalid"
                    )
                }
            )

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        disallowed_fields = request.data.keys() - serializer.fields.keys()

        if disallowed_fields:
            return BadRequestResponse()
        elif not serializer.data:
            return Response(status=status.HTTP_204_NO_CONTENT)

        verwerkingsacties = self.get_queryset().filter(verwerkingId=verwerking_id)

        for verwerkingsactie in verwerkingsacties:
            self.check_object_permissions(self.request, verwerkingsactie)

        if verwerkingsacties and all(
            (verwerkingsactie.vervallen for verwerkingsactie in verwerkingsacties)
        ):
            return BadRequestResponse(
                vervallen=ErrorDetail(
                    _("Opgevraagde verwerkingsacties zijn vervallen."), code="invalid"
                )
            )

        for verwerkingsactie in verwerkingsacties.active():
            self.check_object_permissions(self.request, verwerkingsactie)

        if len(verwerkingsacties.active()) == 0:
            return NotFoundResponse()

        for verwerkingsactie in verwerkingsacties.active():
            verwerkingsactie.bewaartermijn = serializer.validated_data.get(
                "bewaartermijn", verwerkingsactie.bewaartermijn
            )
            verwerkingsactie.vertrouwelijkheid = serializer.validated_data.get(
                "vertrouwelijkheid", verwerkingsactie.vertrouwelijkheid
            )

            previous_actieId = verwerkingsactie.actieId
            verwerkingsactie.actieId = uuid.uuid4()
            verwerkingsactie.save(previous_actieId=previous_actieId, reset_version=True)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """
        Creates a new Verwerkingsactie if the provided data passes validation.
        This operation mixes behavior from DRF's update & create methods because
        a new Verwerkingsactie is created.
        """
        instances = self.get_queryset().filter(
            **{self.lookup_field: self.kwargs[self.lookup_field]}
        )

        if len(instances) == 0:
            return NotFoundResponse()

        instance = instances.first()

        if not instance.is_active:
            return BadRequestResponse(
                vervallen=ErrorDetail(
                    _("Opgevraagde verwerkingsactie is vervallen."), code="invalid"
                )
            )

        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data.update(versie=1, actieId=uuid.uuid4())
        self.perform_update(serializer)

        # follow default django behavior
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instances = self.get_queryset().filter(actieId=self.kwargs["actieId"])

        if instances.count() < 1:
            raise NotFound()

        instance = instances.active().first()

        if not instance:
            return BadRequestResponse(
                vervallen=ErrorDetail(
                    _("Verwerkingsactie is al vervallen."), code="invalid"
                )
            )

        self.check_object_permissions(self.request, instance)
        self.perform_destroy(instance)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        """
        Sets the `vervallen` and `tijdstipVerwijderd` fields and
        creates a new instance (during `.save`).
        """
        instance.vervallen = True
        instance.tijdstipVerwijderd = timezone.now()
        instance.save()


class VerwerktObjectViewSet(VertrouwelijkheidsViewMixin, viewsets.ReadOnlyModelViewSet):
    queryset = VerwerktObject.objects.all()

    lookup_field = "verwerktObjectId"
    vertrouwelijkheid_lookup = "verwerkingsactie__vertrouwelijkheid"

    serializer_class = VerwerktObjectSerializer
    filterset_class = VerwerkteObjectenFilter

    permission_classes = (VerwerktObjectPermissions,)

    permissions = {
        "GET": [Permission.READ_NORMAL, Permission.READ_CONFIDENTIAL],
    }

    def _get_object(self, queryset, filter_kwargs):
        return get_object_or_404(
            queryset, verwerkingsactie__vervallen=False, **filter_kwargs
        )

    def get_queryset(self):
        return super().get_queryset().filter(verwerkingsactie__vervallen=False)


def custom404_handler(request, exception=None):
    return NotFoundResponse()


def custom500_handler(request):
    return InternalServerErrorResponse()
