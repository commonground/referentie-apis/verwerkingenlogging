import json
import logging
from http import HTTPStatus

from django.http import HttpResponse

from rest_framework.views import exception_handler

logger = logging.getLogger(__name__)


def serialize_error(name, error):
    return {
        "name": name,
        "code": error.code,
        "reason": str(error),
    }


class ErrorResponse(HttpResponse):
    """
    :meta private:
    """

    def __init__(self, **extra_data):
        super().__init__(
            content=self.render_content(extra_data),
            content_type="application/problem+json",
        )

    def render_content(self, extra_data):
        data = {
            "type": self.Meta.type,
            "code": HTTPStatus(self.status_code).name,
            "title": self.Meta.title,
            "status": self.Meta.status,
            "detail": "",
            "instance": self.Meta.instance,
        }

        if len(extra_data) > 0:
            data = {**data, **extra_data}

        return json.dumps(data).encode()

    @property
    def status_code(self):
        return self.Meta.status


class BadRequestResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 400 antwoord
    """

    class Meta:
        type = "uri"
        title = "Verkeerde aanvraag"
        status = 400
        instance = "uri"

    def __init__(self, **kwargs):
        invalid_params = {}

        for (name, errors) in kwargs.items():
            if isinstance(errors, list):
                for err in errors:
                    if isinstance(err, dict):
                        for (name_child_object, errors_child_object) in err.items():
                            for error_child_object in errors_child_object:
                                object_name = "{}.{}".format(name, name_child_object)

                                invalid_params[object_name] = serialize_error(
                                    object_name, error_child_object
                                )
                    else:
                        invalid_params[name] = serialize_error(name, err)
            else:
                invalid_params[name] = serialize_error(name, errors)

        super().__init__(invalidParams=invalid_params)


class NotFoundResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 404 antwoord
    """

    class Meta:
        type = "uri"
        title = "Aanvraag onbekend"
        status = 404
        instance = "uri"


class UnauthorizedResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 401 antwoord
    """

    class Meta:
        type = "uri"
        title = "Geen authorisatie"
        status = 401
        instance = "uri"


class ForbiddenResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 403 antwoord
    """

    class Meta:
        type = "uri"
        title = "Verboden"
        status = 403
        instance = "uri"


class MethodNotAllowedResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 405 antwoord
    """

    class Meta:
        type = "uri"
        title = "Methode niet toegestaan"
        status = 405
        instance = "uri"


class NotAcceptableResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 406 antwoord
    """

    class Meta:
        type = "uri"
        title = "Niet acceptabel"
        status = 406
        instance = "uri"


class ConflictResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 409 antwoord
    """

    class Meta:
        type = "uri"
        title = "Er is een conflict"
        status = 409
        instance = "uri"


class GoneResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 410 antwoord
    """

    class Meta:
        type = "uri"
        title = "De bron is niet langer beschikbaar"
        status = 410
        instance = "uri"


class UnsupportedMediaTypeResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 415 antwoord
    """

    class Meta:
        type = "uri"
        title = "Verzoekt kan niet worden geaccepteerd. Inhoud formaat wordt niet ondersteund"
        status = 415
        instance = "uri"


class TooManyRequestResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 429 antwoord
    """

    class Meta:
        type = "uri"
        title = "Er zijn teveel verzoeken uitgevoerd"
        status = 429
        instance = "uri"


class InternalServerErrorResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 500 antwoord
    """

    class Meta:
        type = "uri"
        title = "Server fout"
        status = 500
        instance = "uri"


class NotImplementedResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 501 antwoord
    """

    class Meta:
        type = "uri"
        title = "Actie is niet geïmplementeerd"
        status = 501
        instance = "uri"


class ServiceUnavailableResponse(ErrorResponse):
    """
    JSON problem details representatie van een HTTP 503 antwoord
    """

    class Meta:
        type = "uri"
        title = "Dienst niet beschikbaar"
        status = 503
        instance = "uri"


def handler(exc, context):
    response = exception_handler(exc, context)
    status_code = response.status_code if response is not None else 500

    if status_code == 400:
        if isinstance(response.data, list):
            return BadRequestResponse(algemeen=response.data)

        return BadRequestResponse(**response.data)

    if status_code == 401:
        return UnauthorizedResponse()

    if status_code == 403:
        return ForbiddenResponse()

    if status_code == 404:
        return NotFoundResponse()

    if status_code == 405:
        return MethodNotAllowedResponse()

    if status_code == 406:
        return NotAcceptableResponse()

    if status_code == 409:
        return ConflictResponse()

    if status_code == 410:
        return GoneResponse()

    if status_code == 415:
        return UnsupportedMediaTypeResponse()

    if status_code == 429:
        return TooManyRequestResponse()

    if status_code == 500:
        logger.error(exc)

        return InternalServerErrorResponse()

    if status_code == 501:
        return NotImplementedResponse()

    if status_code == 503:
        return ServiceUnavailableResponse()

    return response
