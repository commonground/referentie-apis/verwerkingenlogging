from django.utils.translation import gettext as _

from django_filters import rest_framework as filters
from django_filters.fields import MultipleChoiceField
from vng_api_common.filters import Backend

from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class CustomMultipleChoiceField(MultipleChoiceField):
    def to_python(self, raw_value):
        if not raw_value:
            return []

        return raw_value[0].split(",")


class OrChoiceFilter(filters.MultipleChoiceFilter):
    field_class = CustomMultipleChoiceField


class VerwerkingsactiesFilter(filters.FilterSet):
    objecttype = filters.CharFilter(
        field_name="verwerkteObjecten__objecttype",
        help_text=_("Objecttype van een VERWERKTOBJECT."),
    )
    soortObjectId = filters.CharFilter(
        field_name="verwerkteObjecten__soortObjectId",
        help_text=_("SoortObjectId van een VERWERKTOBJECT."),
    )
    objectId = filters.CharFilter(
        field_name="verwerkteObjecten__objectId",
        help_text=_("ObjectId van een VERWERKTOBJECT."),
    )
    beginDatum = filters.DateFilter(
        field_name="tijdstip",
        lookup_expr="gte",
        help_text=_("Begin datum van een VERWERKINGSACTIE."),
    )
    eindDatum = filters.DateFilter(
        field_name="tijdstip",
        lookup_expr="lte",
        help_text=_("Eind datum van een VERWERKINGSACTIE."),
    )
    vertrouwelijkheid = OrChoiceFilter(
        choices=Verwerkingsactie.Vertrouwelijkheid.choices,
        field_name="vertrouwelijkheid",
        help_text=_("Vertrouwelijkheid van een VERWERKINGSACTIE."),
    )

    class Meta:
        model = Verwerkingsactie
        fields = (
            "objecttype",
            "soortObjectId",
            "objectId",
            "beginDatum",
            "eindDatum",
            "vertrouwelijkheid",
            "verwerkingsactiviteitId",
        )


class VerwerkingsactiesListFilter(VerwerkingsactiesFilter):
    objecttype = filters.CharFilter(
        field_name="verwerkteObjecten__objecttype",
        help_text=_("Objecttype van een VERWERKTOBJECT."),
        required=True,
    )
    soortObjectId = filters.CharFilter(
        field_name="verwerkteObjecten__soortObjectId",
        help_text=_("SoortObjectId van een VERWERKTOBJECT."),
        required=True,
    )
    objectId = filters.CharFilter(
        field_name="verwerkteObjecten__objectId",
        help_text=_("ObjectId van een VERWERKTOBJECT."),
        required=True,
    )


class VerwerkingsactieFilterBackend(Backend):
    """
    A backend which uses a different filter for the `list` operation
    """

    def get_filter_class(self, view, queryset=None):
        default_class = super().get_filter_class(view, queryset=queryset)

        if default_class and view.action == "list":
            return VerwerkingsactiesListFilter

        return default_class


class VerwerkteObjectenFilter(filters.FilterSet):
    beginDatum = filters.DateFilter(
        field_name="verwerkingsactie__tijdstip",
        lookup_expr="gte",
        help_text=_("Begin datum van een VERWERKINGSACTIE."),
    )
    eindDatum = filters.DateFilter(
        field_name="verwerkingsactie__tijdstip",
        lookup_expr="lte",
        help_text=_("Begin datum van een VERWERKINGSACTIE."),
    )

    verwerkingsactiviteitId = filters.CharFilter(
        field_name="verwerkingsactie__verwerkingsactiviteitId",
        help_text=_(
            "Het ID van het verwerkingsactiviteit gekoppeld aan een VERWERKINGSACTIE."
        ),
    )

    class Meta:
        model = VerwerktObject
        fields = (
            "objecttype",
            "soortObjectId",
            "objectId",
        )
