from datetime import timedelta
from uuid import uuid4

from django.conf import settings
from django.utils import timezone as django_timezone

import pytz
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject, VerwerktSoortGegeven


class CustomHeaderTestCase(APITestCase):
    def setUp(self):
        token = get_jwt_token(
            scopes=[
                "read:confidential",
                "create:confidential",
                "update:confidential",
                "delete:confidential",
            ]
        )
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

    def test_add_api_version_in_header_GET(self):
        response = self.client.get(reverse("verwerkingsactie-list"))
        self.assertEqual(response.headers["API-Version"], settings.API_VERSION)

    def test_add_api_version_in_header_POST(self):
        POST_DATA = {
            "actieNaam": "Zoeken personen",
            "handelingNaam": "Intake",
            "verwerkingNaam": "Huwelijk",
            "verwerkingId": "48086bf2-11b7-4603-9526-67d7c3bb6587",
            "verwerkingsactiviteitId": "5f0bef4c-f66f-4311-84a5-19e8bf359eaf",
            "verwerkingsactiviteitUrl": "https://loggingapi.vng.cloud/api/v1/verwerkingsactiviteiten/5f0bef4c-f66f-4311-84a5-19e8bf359eaf",
            "vertrouwelijkheid": "normaal",
            "bewaartermijn": "P10Y",
            "uitvoerder": "00000001821002193000",
            "systeem": "FooBarApp v2.1",
            "gebruiker": "123456789",
            "gegevensbron": "FooBar Database Publiekszaken",
            "soortAfnemerId": "OIN",
            "afnemerId": "00000001821002193000",
            "verwerkingsactiviteitIdAfnemer": "c5b9f4e7-8c79-41b9-91e2-6268419cb167",
            "verwerkingsactiviteitUrlAfnemer": "https://www.amsterdam.nl/var/api/v1/verwerkingsactiviteiten/5f0bef4c-f66f-4311-84a5-19e8bf359eaf",
            "verwerkingIdAfnemer": "4b698de3-ffba-45e7-8697-a283ec863db2",
            "tijdstip": "2024-04-05T14:35:42+01:00",
            "verwerkteObjecten": [
                {
                    "objecttype": "persoon",
                    "soortObjectId": "BSN",
                    "objectId": "1234567",
                    "betrokkenheid": "Getuige",
                    "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
                }
            ],
        }

        response = self.client.post(
            reverse("verwerkingsactie-list"),
            data=POST_DATA,
            format="json",
        )
        self.assertEqual(response.headers["API-Version"], settings.API_VERSION)

    def test_add_api_version_in_header_PUT(self):
        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie_1 = Verwerkingsactie.objects.create(**verwerkingsactie_data)
        verwerkingsactie_2 = Verwerkingsactie.objects.create(
            **{**verwerkingsactie_data, "systeem": "Systeem Y"}, versie=2
        )

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie_1,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie_2,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)
        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **{**verwerkingsactie_data, "systeem": "Systeem Z"},
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )
        self.assertEqual(response.headers["API-Version"], settings.API_VERSION)

    def test_add_api_version_in_header_PATCH(self):
        verwerkingId = str(uuid4())

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        self.assertEqual(response.headers["API-Version"], settings.API_VERSION)

    def test_add_api_version_in_header_DELETE(self):
        verwerkingsactie = Verwerkingsactie.objects.create(
            tijdstip=django_timezone.now(),
            versie=1,
        )

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingsactie.actieId),
                ),
            ),
        )
        self.assertEqual(response.headers["API-Version"], settings.API_VERSION)
