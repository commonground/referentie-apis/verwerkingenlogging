from datetime import timedelta
from uuid import uuid4

from django.conf import settings
from django.utils import timezone as django_timezone

import pytz
from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests.urls import reverse

from vlc.api.tests.factories import VerwerktObjectFactory
from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class VerwerkingsactieRetrieveTestCase(APITestCase):
    def test_retrieve(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactie = Verwerkingsactie.objects.create(
            actieId=uuid4(),
            tijdstip=django_timezone.now(),
        )
        verwerktobject = VerwerktObjectFactory(verwerkingsactie=verwerkingsactie)

        url = reverse(
            "verwerkingsactie-detail",
            kwargs=dict(actieId=str(verwerkingsactie.actieId)),
        )

        verwerkte_objecten_url = reverse(
            "verwerktobject-detail",
            kwargs=dict(verwerktObjectId=str(verwerktobject.verwerktObjectId)),
        )

        response = self.client.get(url)

        timezone = pytz.timezone(settings.TIME_ZONE)

        self.maxDiff = None

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                "url": f"http://testserver{url}",
                "actieId": str(verwerkingsactie.actieId),
                "actieNaam": None,
                "handelingNaam": None,
                "verwerkingNaam": None,
                "verwerkingId": None,
                "verwerkingsactiviteitId": None,
                "verwerkingsactiviteitUrl": None,
                "vertrouwelijkheid": verwerkingsactie.vertrouwelijkheid.value,
                "bewaartermijn": None,
                "uitvoerder": None,
                "systeem": None,
                "gebruiker": None,
                "gegevensbron": None,
                "soortAfnemerId": None,
                "afnemerId": None,
                "verwerkingsactiviteitIdAfnemer": None,
                "verwerkingsactiviteitUrlAfnemer": None,
                "verwerkingIdAfnemer": None,
                "tijdstip": verwerkingsactie.tijdstip.astimezone(timezone).isoformat(),
                "tijdstipRegistratie": verwerkingsactie.tijdstipRegistratie.astimezone(
                    timezone
                ).isoformat(),
                "verwerkteObjecten": [
                    {
                        "url": (f"http://testserver{verwerkte_objecten_url}"),
                        "verwerktObjectId": str(verwerktobject.verwerktObjectId),
                        "objecttype": VerwerktObject.Objecttype.PERSOON.value,
                        "soortObjectId": "BSN",
                        "objectId": None,
                        "betrokkenheid": None,
                        "verwerkteSoortenGegevens": [
                            {
                                "soortGegeven": "BSN",
                            }
                        ],
                    }
                ],
            },
        )

    def test_retrieve_multiple_actieid(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        actieId = uuid4()

        Verwerkingsactie.objects.create(
            actieId=actieId,
            tijdstip=django_timezone.now() - timedelta(days=1),
            versie=1,
        )

        expected_verwerkingsactie = Verwerkingsactie.objects.create(
            actieId=actieId,
            tijdstip=django_timezone.now(),
            versie=2,
        )

        response = self.client.get(
            reverse("verwerkingsactie-detail", kwargs=dict(actieId=str(actieId))),
        )

        timezone = pytz.timezone(settings.TIME_ZONE)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json()["tijdstipRegistratie"],
            expected_verwerkingsactie.tijdstipRegistratie.astimezone(
                timezone
            ).isoformat(),
        )

    def test_retrieve_inactive_verwerkingsactie(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        Verwerkingsactie.objects.create(
            tijdstip=django_timezone.now() - timedelta(days=3),
            tijdstipVerwijderd=django_timezone.now() - timedelta(days=2),
        )

        verwerkingsactie = Verwerkingsactie.objects.create(
            vervallen=True,
            tijdstip=django_timezone.now() - timedelta(days=1),
            tijdstipVerwijderd=django_timezone.now() - timedelta(days=1),
        )

        response = self.client.get(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(actieId=str(verwerkingsactie.actieId)),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_unknown(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        response = self.client.get(
            reverse("verwerkingsactie-detail", kwargs=dict(actieId=str(uuid4()))),
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
