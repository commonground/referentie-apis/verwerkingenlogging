from datetime import timedelta
from uuid import uuid4

from django.utils import timezone as django_timezone
from django.utils.translation import gettext as _

from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.tests.factories import (
    VerwerkingsactieFactory,
    VerwerktObjectFactory,
    VerwerktSoortGegevenFactory,
)
from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class VerwerkingsactiePartialUpdateTestCase(APITestCase):
    def test_partial_update(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        actieId = str(uuid4())
        verwerkingsactie = Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            actieId=actieId,
            tijdstip=django_timezone.now(),
            versie=1,
        )

        verwerktObjectId = uuid4()
        verwerktSoortGegeven = VerwerktSoortGegevenFactory()
        verwerktObject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingsactie,
            objecttype=VerwerktObject.Objecttype.PERSOON.value,
            soortObjectId="Object soort X",
            objectId="XYZ",
            betrokkenheid="Eigenaar",
            verwerkteSoortenGegevens=[verwerktSoortGegeven],
            verwerktObjectId=verwerktObjectId,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        verwerkingsactie = Verwerkingsactie.objects.exclude(actieId=actieId).get()
        verwerktobject = verwerkingsactie.verwerkteObjecten.get()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.assertEqual(verwerkingsactie.versie, 1)
        self.assertEqual(
            verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK.value,
        )
        self.assertNotEqual(verwerkingsactie.actieId, actieId)

        self.assertEqual(
            verwerktobject.objecttype, VerwerktObject.Objecttype.PERSOON.value
        )
        self.assertEqual(verwerktobject.soortObjectId, "Object soort X")
        self.assertEqual(verwerktobject.objectId, "XYZ")
        self.assertEqual(verwerktobject.betrokkenheid, "Eigenaar")
        self.assertCountEqual(
            verwerktobject.verwerkteSoortenGegevens.all(), [verwerktSoortGegeven]
        )

        self.assertNotEqual(verwerktobject.verwerktObjectId, verwerktObjectId)

    def test_partial_update_last_version(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        actieId = str(uuid4())
        Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            actieId=actieId,
            tijdstip=django_timezone.now(),
            systeem="Systeem X",
            versie=1,
        )

        Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            actieId=actieId,
            tijdstip=django_timezone.now(),
            systeem="Systeem Y",
            versie=2,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        verwerkingsactie = Verwerkingsactie.objects.exclude(actieId=actieId).get()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK.value,
        )
        self.assertEqual(verwerkingsactie.versie, 1)
        self.assertEqual(verwerkingsactie.systeem, "Systeem Y")
        self.assertNotEqual(verwerkingsactie.actieId, actieId)

    def test_partial_update_multiple_matches(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())

        tijdstip_a = django_timezone.now() - timedelta(days=2)
        verwerkingsactie_a = Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            tijdstip=tijdstip_a,
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )

        tijdstip_b = django_timezone.now() - timedelta(days=1)
        verwerkingsactie_b = Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            tijdstip=tijdstip_b,
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )

        verwerktSoortGegeven = VerwerktSoortGegevenFactory()

        verwerktObject_a = VerwerktObjectFactory(
            verwerkingsactie=verwerkingsactie_a,
            objecttype=VerwerktObject.Objecttype.PERSOON.value,
            soortObjectId="Object soort X",
            objectId="XYZ",
            betrokkenheid="Eigenaar",
            verwerkteSoortenGegevens=[verwerktSoortGegeven],
        )

        verwerktObject_b = VerwerktObjectFactory(
            verwerkingsactie=verwerkingsactie_b,
            objecttype=VerwerktObject.Objecttype.PERSOON.value,
            soortObjectId="Object soort Y",
            objectId="ZYX",
            betrokkenheid="Architect",
            verwerkteSoortenGegevens=[verwerktSoortGegeven],
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        # fmt:off
        new_verwerkingsactie_a = (
            Verwerkingsactie.objects.filter(
                verwerkingId=verwerkingId,
                tijdstip=tijdstip_a,
            )
            .exclude(actieId=verwerkingsactie_a.actieId)
            .first()
        )
        # fmt:on

        new_verwerktobject_a = new_verwerkingsactie_a.verwerkteObjecten.get()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            new_verwerkingsactie_a.vertrouwelijkheid,
            new_verwerkingsactie_a.Vertrouwelijkheid.VERTROUWELIJK.value,
        )
        self.assertNotEqual(new_verwerkingsactie_a.actieId, verwerkingsactie_a.actieId)

        self.assertEqual(
            new_verwerktobject_a.objecttype, VerwerktObject.Objecttype.PERSOON.value
        )
        self.assertEqual(new_verwerktobject_a.soortObjectId, "Object soort X")
        self.assertEqual(new_verwerktobject_a.objectId, "XYZ")
        self.assertEqual(new_verwerktobject_a.betrokkenheid, "Eigenaar")
        self.assertCountEqual(
            new_verwerktobject_a.verwerkteSoortenGegevens.all(), [verwerktSoortGegeven]
        )

        self.assertNotEqual(
            new_verwerktobject_a.verwerktObjectId, verwerktObject_a.verwerktObjectId
        )

        # fmt:off
        new_verwerkingsactie_b = (
            Verwerkingsactie.objects.filter(
                verwerkingId=verwerkingId,
                tijdstip=tijdstip_b,
            )
            .exclude(actieId=verwerkingsactie_b.actieId)
            .first()
        )
        # fmt:on

        new_verwerktobject_b = new_verwerkingsactie_b.verwerkteObjecten.get()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            new_verwerkingsactie_b.vertrouwelijkheid,
            new_verwerkingsactie_b.Vertrouwelijkheid.VERTROUWELIJK.value,
        )
        self.assertNotEqual(new_verwerkingsactie_b.actieId, verwerkingsactie_b.actieId)

        self.assertEqual(
            new_verwerktobject_b.objecttype, VerwerktObject.Objecttype.PERSOON.value
        )
        self.assertEqual(new_verwerktobject_b.soortObjectId, "Object soort Y")
        self.assertEqual(new_verwerktobject_b.objectId, "ZYX")
        self.assertEqual(new_verwerktobject_b.betrokkenheid, "Architect")
        self.assertCountEqual(
            new_verwerktobject_b.verwerkteSoortenGegevens.all(), [verwerktSoortGegeven]
        )

        self.assertNotEqual(
            new_verwerktobject_b.verwerktObjectId, verwerktObject_b.verwerktObjectId
        )

    def test_partial_update_inactive(self):
        """
        Test that if the most recent Verwerkingsactie is inactive, the patch
        should not be possible.
        """

        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = uuid4()

        default_kwargs = {
            "verwerkingId": verwerkingId,
            "actieId": uuid4(),
            "tijdstip": django_timezone.now(),
        }

        Verwerkingsactie.objects.create(
            **default_kwargs,
            versie=1,
        )

        Verwerkingsactie.objects.create(
            **default_kwargs,
            vervallen=True,
            versie=2,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        error = response.json()["invalidParams"]["vervallen"]

        self.assertEqual(
            error["reason"], _("Opgevraagde verwerkingsacties zijn vervallen.")
        )

    def test_partial_update_ignore_inactive(self):
        """
        Test that inactive verwerkingsacties are ignored
        """

        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = uuid4()

        inactive_default_kwargs = {
            "verwerkingId": verwerkingId,
            "actieId": uuid4(),
            "tijdstip": django_timezone.now(),
        }

        Verwerkingsactie.objects.create(
            **inactive_default_kwargs,
            versie=1,
        )

        Verwerkingsactie.objects.create(
            **inactive_default_kwargs,
            vervallen=True,
            versie=2,
        )

        verwerkingsactie = Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            tijdstip=django_timezone.now(),
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            versie=1,
        )

        verwerktObjectId = uuid4()
        verwerktSoortGegeven = VerwerktSoortGegevenFactory()
        VerwerktObjectFactory(
            verwerkingsactie=verwerkingsactie,
            objecttype=VerwerktObject.Objecttype.PERSOON.value,
            soortObjectId="Object soort X",
            objectId="XYZ",
            betrokkenheid="Eigenaar",
            verwerkteSoortenGegevens=[verwerktSoortGegeven],
            verwerktObjectId=verwerktObjectId,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        self.assertEqual(Verwerkingsactie.objects.count(), 4)

        # fmt:off
        new_verwerkingsactie = (
            Verwerkingsactie.objects
            .filter(verwerkingId=verwerkingId)
            .exclude(actieId=verwerkingsactie.actieId)
            .active()
            .first()
        )
        # fmt:on

        verwerktobject = new_verwerkingsactie.verwerkteObjecten.get()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            new_verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK.value,
        )
        self.assertNotEqual(new_verwerkingsactie.actieId, verwerkingsactie.actieId)

        self.assertEqual(
            verwerktobject.objecttype, VerwerktObject.Objecttype.PERSOON.value
        )
        self.assertEqual(verwerktobject.soortObjectId, "Object soort X")
        self.assertEqual(verwerktobject.objectId, "XYZ")
        self.assertEqual(verwerktobject.betrokkenheid, "Eigenaar")
        self.assertCountEqual(
            verwerktobject.verwerkteSoortenGegevens.all(), [verwerktSoortGegeven]
        )

        self.assertNotEqual(verwerktobject.verwerktObjectId, verwerktObjectId)

    def test_partial_update_without_vertrouwelijkheid(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        verwerkingsactie = VerwerkingsactieFactory(verwerkingId=verwerkingId)

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "bewaartermijn": "P20Y",
            },
            format="json",
        )

        # fmt: off
        verwerkingsactie = (
            Verwerkingsactie.objects.exclude(actieId=verwerkingsactie.actieId)
            .get()
        )
        # fmt: on

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(verwerkingsactie.bewaartermijn, "P20Y")

    def test_partial_update_unknown(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = uuid4()
        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_partial_update_disallowed_field(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        actieId = str(uuid4())
        Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            actieId=actieId,
            tijdstip=django_timezone.now(),
            versie=1,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "systeem": "Systeem X",
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Verwerkingsactie.objects.count(), 1)

    def test_partial_update_same_value(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        actieId = str(uuid4())
        verwerkingsactie = Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            actieId=actieId,
            tijdstip=django_timezone.now(),
            bewaartermijn="P20Y",
            versie=1,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "bewaartermijn": "P20Y",
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        verwerkingsactie = Verwerkingsactie.objects.exclude(actieId=actieId).get()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.assertEqual(verwerkingsactie.versie, 1)

    def test_partial_update_empty(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = uuid4()
        actieId = str(uuid4())
        Verwerkingsactie.objects.create(
            verwerkingId=verwerkingId,
            actieId=actieId,
            tijdstip=django_timezone.now(),
            versie=1,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {},
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
