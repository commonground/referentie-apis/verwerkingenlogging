from datetime import timedelta
from uuid import uuid4

from django.conf import settings
from django.utils import timezone as django_timezone

import pytz
from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class VerwerkingsactieCreateTestCase(APITestCase):
    def test_create(self):
        token = get_jwt_token(scopes=["create:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkte_object = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
        }

        response = self.client.post(
            reverse("verwerkingsactie-list"),
            {
                "actieNaam": "Actie XYZ",
                "afnemerId": "Afnemer Y",
                "bewaartermijn": "P3Y6M4DT12H30M5S",
                "handelingNaam": "Naam Y",
                "soortAfnemerId": "BSN",
                "tijdstip": tijdstip.astimezone(timezone).isoformat(),
                "uitvoerder": "10000000000000000000",
                "systeem": "Systeem X",
                "gebruiker": "Gebruiker Y",
                "gegevensbron": "Bron Z",
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
                "verwerkingId": str(verwerkingId),
                "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
                "verwerkingNaam": "Verwerking X",
                "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
                "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
                "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
                "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
                "verwerkteObjecten": [verwerkte_object],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        verwerkingsactie = Verwerkingsactie.objects.get()

        self.assertEqual(verwerkingsactie.actieNaam, "Actie XYZ")
        self.assertEqual(verwerkingsactie.afnemerId, "Afnemer Y")
        self.assertEqual(verwerkingsactie.bewaartermijn, "P3Y6M4DT12H30M5S")
        self.assertEqual(verwerkingsactie.handelingNaam, "Naam Y")
        self.assertEqual(verwerkingsactie.soortAfnemerId, "BSN")
        self.assertEqual(verwerkingsactie.tijdstip, tijdstip)
        self.assertEqual(verwerkingsactie.uitvoerder, "10000000000000000000")
        self.assertEqual(verwerkingsactie.systeem, "Systeem X")
        self.assertEqual(verwerkingsactie.gebruiker, "Gebruiker Y")
        self.assertEqual(verwerkingsactie.gegevensbron, "Bron Z")
        self.assertEqual(
            verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )
        self.assertEqual(verwerkingsactie.verwerkingId, verwerkingId)
        self.assertEqual(verwerkingsactie.verwerkingIdAfnemer, verwerkingIdAfnemer)
        self.assertEqual(verwerkingsactie.verwerkingNaam, "Verwerking X")
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitId, verwerkingsactiviteitId
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitIdAfnemer,
            verwerkingsactiviteitIdAfnemer,
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitUrl, verwerkingsactiviteitUrl
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitUrlAfnemer,
            verwerkingsactiviteitUrlAfnemer,
        )

        self.assertCountEqual(
            response.json().keys(),
            (
                "url",
                "actieId",
                "actieNaam",
                "handelingNaam",
                "verwerkingNaam",
                "verwerkingId",
                "verwerkingsactiviteitId",
                "verwerkingsactiviteitUrl",
                "vertrouwelijkheid",
                "bewaartermijn",
                "uitvoerder",
                "systeem",
                "gebruiker",
                "gegevensbron",
                "soortAfnemerId",
                "afnemerId",
                "verwerkingsactiviteitIdAfnemer",
                "verwerkingsactiviteitUrlAfnemer",
                "verwerkingIdAfnemer",
                "tijdstip",
                "tijdstipRegistratie",
                "verwerkteObjecten",
            ),
        )

    def test_optional_verwerktesoortengegevens(self):
        token = get_jwt_token(scopes=["create:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkte_object = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
        }

        response = self.client.post(
            reverse("verwerkingsactie-list"),
            {
                "actieNaam": "Actie XYZ",
                "afnemerId": "Afnemer Y",
                "bewaartermijn": "P3Y6M4DT12H30M5S",
                "handelingNaam": "Naam Y",
                "soortAfnemerId": "BSN",
                "tijdstip": tijdstip.astimezone(timezone).isoformat(),
                "uitvoerder": "10000000000000000000",
                "systeem": "Systeem X",
                "gebruiker": "Gebruiker Y",
                "gegevensbron": "Bron Z",
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
                "verwerkingId": str(verwerkingId),
                "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
                "verwerkingNaam": "Verwerking X",
                "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
                "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
                "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
                "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
                "verwerkteObjecten": [verwerkte_object],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        verwerkingsactie = Verwerkingsactie.objects.get()

        self.assertEqual(verwerkingsactie.actieNaam, "Actie XYZ")
        self.assertEqual(verwerkingsactie.afnemerId, "Afnemer Y")
        self.assertEqual(verwerkingsactie.bewaartermijn, "P3Y6M4DT12H30M5S")
        self.assertEqual(verwerkingsactie.handelingNaam, "Naam Y")
        self.assertEqual(verwerkingsactie.soortAfnemerId, "BSN")
        self.assertEqual(verwerkingsactie.tijdstip, tijdstip)
        self.assertEqual(verwerkingsactie.uitvoerder, "10000000000000000000")
        self.assertEqual(verwerkingsactie.systeem, "Systeem X")
        self.assertEqual(verwerkingsactie.gebruiker, "Gebruiker Y")
        self.assertEqual(verwerkingsactie.gegevensbron, "Bron Z")
        self.assertEqual(
            verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )
        self.assertEqual(verwerkingsactie.verwerkingId, verwerkingId)
        self.assertEqual(verwerkingsactie.verwerkingIdAfnemer, verwerkingIdAfnemer)
        self.assertEqual(verwerkingsactie.verwerkingNaam, "Verwerking X")
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitId, verwerkingsactiviteitId
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitIdAfnemer,
            verwerkingsactiviteitIdAfnemer,
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitUrl, verwerkingsactiviteitUrl
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitUrlAfnemer,
            verwerkingsactiviteitUrlAfnemer,
        )

        verwerkteObject = verwerkingsactie.verwerkteObjecten.get()

        self.assertCountEqual(verwerkteObject.verwerkteSoortenGegevens.all(), [])

        self.assertCountEqual(
            response.json().keys(),
            (
                "url",
                "actieId",
                "actieNaam",
                "handelingNaam",
                "verwerkingNaam",
                "verwerkingId",
                "verwerkingsactiviteitId",
                "verwerkingsactiviteitUrl",
                "vertrouwelijkheid",
                "bewaartermijn",
                "uitvoerder",
                "systeem",
                "gebruiker",
                "gegevensbron",
                "soortAfnemerId",
                "afnemerId",
                "verwerkingsactiviteitIdAfnemer",
                "verwerkingsactiviteitUrlAfnemer",
                "verwerkingIdAfnemer",
                "tijdstip",
                "tijdstipRegistratie",
                "verwerkteObjecten",
            ),
        )

    def test_optional_bewaartermijn(self):
        token = get_jwt_token(scopes=["create:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkte_object = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
        }

        response = self.client.post(
            reverse("verwerkingsactie-list"),
            {
                "actieNaam": "Actie XYZ",
                "afnemerId": "Afnemer Y",
                "handelingNaam": "Naam Y",
                "soortAfnemerId": "BSN",
                "tijdstip": tijdstip.astimezone(timezone).isoformat(),
                "uitvoerder": "10000000000000000000",
                "systeem": "Systeem X",
                "gebruiker": "Gebruiker Y",
                "gegevensbron": "Bron Z",
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
                "verwerkingId": str(verwerkingId),
                "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
                "verwerkingNaam": "Verwerking X",
                "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
                "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
                "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
                "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
                "verwerkteObjecten": [verwerkte_object],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        verwerkingsactie = Verwerkingsactie.objects.get()

        self.assertEqual(verwerkingsactie.actieNaam, "Actie XYZ")
        self.assertEqual(verwerkingsactie.afnemerId, "Afnemer Y")
        self.assertEqual(verwerkingsactie.bewaartermijn, None)
        self.assertEqual(verwerkingsactie.handelingNaam, "Naam Y")
        self.assertEqual(verwerkingsactie.soortAfnemerId, "BSN")
        self.assertEqual(verwerkingsactie.tijdstip, tijdstip)
        self.assertEqual(verwerkingsactie.uitvoerder, "10000000000000000000")
        self.assertEqual(verwerkingsactie.systeem, "Systeem X")
        self.assertEqual(verwerkingsactie.gebruiker, "Gebruiker Y")
        self.assertEqual(verwerkingsactie.gegevensbron, "Bron Z")
        self.assertEqual(
            verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )
        self.assertEqual(verwerkingsactie.verwerkingId, verwerkingId)
        self.assertEqual(verwerkingsactie.verwerkingIdAfnemer, verwerkingIdAfnemer)
        self.assertEqual(verwerkingsactie.verwerkingNaam, "Verwerking X")
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitId, verwerkingsactiviteitId
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitIdAfnemer,
            verwerkingsactiviteitIdAfnemer,
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitUrl, verwerkingsactiviteitUrl
        )
        self.assertEqual(
            verwerkingsactie.verwerkingsactiviteitUrlAfnemer,
            verwerkingsactiviteitUrlAfnemer,
        )

        self.assertCountEqual(
            response.json().keys(),
            (
                "url",
                "actieId",
                "actieNaam",
                "handelingNaam",
                "verwerkingNaam",
                "verwerkingId",
                "verwerkingsactiviteitId",
                "verwerkingsactiviteitUrl",
                "vertrouwelijkheid",
                "bewaartermijn",
                "uitvoerder",
                "systeem",
                "gebruiker",
                "gegevensbron",
                "soortAfnemerId",
                "afnemerId",
                "verwerkingsactiviteitIdAfnemer",
                "verwerkingsactiviteitUrlAfnemer",
                "verwerkingIdAfnemer",
                "tijdstip",
                "tijdstipRegistratie",
                "verwerkteObjecten",
            ),
        )
