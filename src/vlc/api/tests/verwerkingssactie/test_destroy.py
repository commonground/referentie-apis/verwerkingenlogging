from datetime import timedelta
from uuid import uuid4

from django.utils import timezone as django_timezone
from django.utils.translation import gettext as _

from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie


class VerwerkingsactieDestroyTestCase(APITestCase):
    def test_destroy(self):
        token = get_jwt_token(scopes=["delete:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactie = Verwerkingsactie.objects.create(
            tijdstip=django_timezone.now(),
            versie=1,
        )

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingsactie.actieId),
                ),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        verwerkingsacties = Verwerkingsactie.objects.filter(
            actieId=verwerkingsactie.actieId
        )

        self.assertEqual(verwerkingsacties.count(), 2)

        new_instance = verwerkingsacties.first()

        self.assertEqual(new_instance.vervallen, True)
        self.assertEqual(new_instance.versie, 2)
        self.assertTrue(new_instance.tijdstipVerwijderd)

        old_instance = verwerkingsacties.last()

        self.assertEqual(old_instance.vervallen, False)
        self.assertEqual(old_instance.versie, 1)
        self.assertFalse(old_instance.tijdstipVerwijderd)

    def test_already_destroyed(self):
        token = get_jwt_token(scopes=["delete:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        actieId = uuid4()
        Verwerkingsactie.objects.create(
            versie=1,
            actieId=actieId,
            tijdstip=django_timezone.now(),
        )

        verwerkingsactie = Verwerkingsactie.objects.create(
            versie=2,
            actieId=actieId,
            vervallen=True,
            tijdstip=django_timezone.now(),
            tijdstipVerwijderd=django_timezone.now() - timedelta(days=1),
        )

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingsactie.actieId),
                ),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        error = response.json()["invalidParams"]["vervallen"]

        self.assertEqual(error["reason"], _("Verwerkingsactie is al vervallen."))

    def test_destroy_unknown_actieid(self):
        token = get_jwt_token(scopes=["delete:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(uuid4()),
                ),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
