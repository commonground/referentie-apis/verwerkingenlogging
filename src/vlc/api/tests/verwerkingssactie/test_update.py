from datetime import timedelta
from uuid import uuid4

from django.conf import settings
from django.utils import timezone as django_timezone
from django.utils.translation import gettext as _

import pytz
from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject, VerwerktSoortGegeven


class VerwerkingsactieUpdateTestCase(APITestCase):
    def test_update(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie = Verwerkingsactie.objects.create(**verwerkingsactie_data)

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **{**verwerkingsactie_data, "systeem": "Systeem Y"},
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        verwerkingsacties = Verwerkingsactie.objects.all()
        verwerkteobjecten = VerwerktObject.objects.all()

        self.assertEqual(verwerkingsacties.count(), 2)
        self.assertEqual(verwerkteobjecten.count(), 2)

        new_verwerkingsactie = verwerkingsacties.last_versions().first()

        self.assertEqual(new_verwerkingsactie.versie, 1)
        self.assertTrue(
            new_verwerkingsactie.tijdstipRegistratie
            > verwerkingsactie.tijdstipRegistratie
        )
        self.assertEqual(new_verwerkingsactie.systeem, "Systeem Y")
        self.assertNotEqual(new_verwerkingsactie.actieId, verwerkingsactie.actieId)

        self.assertEqual(new_verwerkingsactie.actieNaam, "Actie XYZ")
        self.assertEqual(new_verwerkingsactie.afnemerId, "Afnemer Y")
        self.assertEqual(new_verwerkingsactie.bewaartermijn, "P3Y6M4DT12H30M5S")
        self.assertEqual(new_verwerkingsactie.handelingNaam, "Naam Y")
        self.assertEqual(new_verwerkingsactie.soortAfnemerId, "BSN")
        self.assertEqual(new_verwerkingsactie.tijdstip, tijdstip)
        self.assertEqual(new_verwerkingsactie.uitvoerder, "10000000000000000000")
        self.assertEqual(new_verwerkingsactie.gebruiker, "Gebruiker Y")
        self.assertEqual(new_verwerkingsactie.gegevensbron, "Bron Z")
        self.assertEqual(
            new_verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )
        self.assertEqual(new_verwerkingsactie.verwerkingId, verwerkingId)
        self.assertEqual(new_verwerkingsactie.verwerkingIdAfnemer, verwerkingIdAfnemer)
        self.assertEqual(new_verwerkingsactie.verwerkingNaam, "Verwerking X")
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitId, verwerkingsactiviteitId
        )
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitIdAfnemer,
            verwerkingsactiviteitIdAfnemer,
        )
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitUrl, verwerkingsactiviteitUrl
        )
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitUrlAfnemer,
            verwerkingsactiviteitUrlAfnemer,
        )

        new_verwerktobject = new_verwerkingsactie.verwerkteObjecten.get()

        self.assertEqual(
            new_verwerktobject.objecttype, VerwerktObject.Objecttype.PERSOON
        )
        self.assertEqual(new_verwerktobject.soortObjectId, "BSN")
        self.assertEqual(new_verwerktobject.objectId, "1234567")
        self.assertEqual(new_verwerktobject.betrokkenheid, "Getuige")
        self.assertCountEqual(
            new_verwerktobject.verwerkteSoortenGegevens.all(),
            [VerwerktSoortGegeven.objects.get()],
        )

        self.assertCountEqual(
            response.json().keys(),
            (
                "url",
                "actieId",
                "actieNaam",
                "handelingNaam",
                "verwerkingNaam",
                "verwerkingId",
                "verwerkingsactiviteitId",
                "verwerkingsactiviteitUrl",
                "vertrouwelijkheid",
                "bewaartermijn",
                "uitvoerder",
                "systeem",
                "gebruiker",
                "gegevensbron",
                "soortAfnemerId",
                "afnemerId",
                "verwerkingsactiviteitIdAfnemer",
                "verwerkingsactiviteitUrlAfnemer",
                "verwerkingIdAfnemer",
                "tijdstip",
                "tijdstipRegistratie",
                "verwerkteObjecten",
            ),
        )

    def test_update_latest_version(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie_1 = Verwerkingsactie.objects.create(**verwerkingsactie_data)
        verwerkingsactie_2 = Verwerkingsactie.objects.create(
            **{**verwerkingsactie_data, "systeem": "Systeem Y"}, versie=2
        )

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie_1,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie_2,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **{**verwerkingsactie_data, "systeem": "Systeem Z"},
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        verwerkingsacties = Verwerkingsactie.objects.all()
        verwerkteobjecten = VerwerktObject.objects.all()

        self.assertEqual(verwerkingsacties.count(), 3)
        self.assertEqual(verwerkteobjecten.count(), 3)

        new_verwerkingsactie = verwerkingsacties.last_versions().first()

        self.assertEqual(new_verwerkingsactie.versie, 1)
        self.assertTrue(
            new_verwerkingsactie.tijdstipRegistratie
            > verwerkingsactie_1.tijdstipRegistratie
        )
        self.assertTrue(
            new_verwerkingsactie.tijdstipRegistratie
            > verwerkingsactie_2.tijdstipRegistratie
        )
        self.assertEqual(new_verwerkingsactie.systeem, "Systeem Z")
        self.assertNotEqual(new_verwerkingsactie.actieId, verwerkingsactie_1.actieId)
        self.assertNotEqual(new_verwerkingsactie.actieId, verwerkingsactie_2.actieId)

        self.assertEqual(new_verwerkingsactie.actieNaam, "Actie XYZ")
        self.assertEqual(new_verwerkingsactie.afnemerId, "Afnemer Y")
        self.assertEqual(new_verwerkingsactie.bewaartermijn, "P3Y6M4DT12H30M5S")
        self.assertEqual(new_verwerkingsactie.handelingNaam, "Naam Y")
        self.assertEqual(new_verwerkingsactie.soortAfnemerId, "BSN")
        self.assertEqual(new_verwerkingsactie.tijdstip, tijdstip)
        self.assertEqual(new_verwerkingsactie.uitvoerder, "10000000000000000000")
        self.assertEqual(new_verwerkingsactie.gebruiker, "Gebruiker Y")
        self.assertEqual(new_verwerkingsactie.gegevensbron, "Bron Z")
        self.assertEqual(
            new_verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )
        self.assertEqual(new_verwerkingsactie.verwerkingId, verwerkingId)
        self.assertEqual(new_verwerkingsactie.verwerkingIdAfnemer, verwerkingIdAfnemer)
        self.assertEqual(new_verwerkingsactie.verwerkingNaam, "Verwerking X")
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitId, verwerkingsactiviteitId
        )
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitIdAfnemer,
            verwerkingsactiviteitIdAfnemer,
        )
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitUrl, verwerkingsactiviteitUrl
        )
        self.assertEqual(
            new_verwerkingsactie.verwerkingsactiviteitUrlAfnemer,
            verwerkingsactiviteitUrlAfnemer,
        )

        new_verwerktobject = new_verwerkingsactie.verwerkteObjecten.get()

        self.assertEqual(
            new_verwerktobject.objecttype, VerwerktObject.Objecttype.PERSOON
        )
        self.assertEqual(new_verwerktobject.soortObjectId, "BSN")
        self.assertEqual(new_verwerktobject.objectId, "1234567")
        self.assertEqual(new_verwerktobject.betrokkenheid, "Getuige")
        self.assertCountEqual(
            new_verwerktobject.verwerkteSoortenGegevens.all(),
            [VerwerktSoortGegeven.objects.get()],
        )

        self.assertCountEqual(
            response.json().keys(),
            (
                "url",
                "actieId",
                "actieNaam",
                "handelingNaam",
                "verwerkingNaam",
                "verwerkingId",
                "verwerkingsactiviteitId",
                "verwerkingsactiviteitUrl",
                "vertrouwelijkheid",
                "bewaartermijn",
                "uitvoerder",
                "systeem",
                "gebruiker",
                "gegevensbron",
                "soortAfnemerId",
                "afnemerId",
                "verwerkingsactiviteitIdAfnemer",
                "verwerkingsactiviteitUrlAfnemer",
                "verwerkingIdAfnemer",
                "tijdstip",
                "tijdstipRegistratie",
                "verwerkteObjecten",
            ),
        )

    def test_update_inactive(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie = Verwerkingsactie.objects.create(
            **verwerkingsactie_data, vervallen=True
        )

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, __ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **{**verwerkingsactie_data, "systeem": "Systeem Y"},
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        error = response.json()["invalidParams"]["vervallen"]

        self.assertEqual(
            error["reason"], _("Opgevraagde verwerkingsactie is vervallen.")
        )

    def test_update_unknown(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **{**verwerkingsactie_data, "systeem": "Systeem Y"},
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        error = response.json()

        self.assertEqual(error["title"], "Aanvraag onbekend")
