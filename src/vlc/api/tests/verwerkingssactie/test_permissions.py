from datetime import timedelta
from urllib.parse import urlencode
from uuid import uuid4

from django.conf import settings
from django.utils import timezone as django_timezone
from django.utils.translation import gettext as _

import pytz
from rest_framework import status
from rest_framework.test import APIRequestFactory, APITestCase
from vng_api_common.tests import reverse

from vlc.api.serializers import VerwerkingsactieSerializer
from vlc.api.tests.factories import VerwerkingsactieFactory, VerwerktObjectFactory
from vlc.api.tests.utils import VersioningTestMixin
from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject, VerwerktSoortGegeven


class VerwerkingsactiePermissionTestCase(VersioningTestMixin, APITestCase):
    def setUp(self):
        self.request_factory = APIRequestFactory()

    def test_retrieve_normal_permission(self):
        token = get_jwt_token(scopes=["read:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        default_kwargs = dict(actieId=str(verwerkingactie.actieId))
        url = reverse("verwerkingsactie-detail", kwargs=default_kwargs)
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request, **default_kwargs)

        serializer = VerwerkingsactieSerializer(
            instance=verwerkingactie,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), serializer.data)

    def test_retrieve_confidential_permission(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )

        default_kwargs = dict(actieId=str(verwerkingactie.actieId))
        url = reverse("verwerkingsactie-detail", kwargs=default_kwargs)
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request, **default_kwargs)

        serializer = VerwerkingsactieSerializer(
            instance=verwerkingactie,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), serializer.data)

    def test_retrieve_confidential_permission_normal_vertrouwelijkheid(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        default_kwargs = dict(actieId=str(verwerkingactie.actieId))
        url = reverse("verwerkingsactie-detail", kwargs=default_kwargs)
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request, **default_kwargs)

        serializer = VerwerkingsactieSerializer(
            instance=verwerkingactie,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), serializer.data)

    def test_retrieve_insufficient_permissions(self):
        token = get_jwt_token(scopes=["read:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )

        response = self.client.get(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingactie.actieId),
                ),
            )
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_normal_permission(self):
        token = get_jwt_token(scopes=["read:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        default_params = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "123456789",
        }

        verwerkt_object = VerwerktObjectFactory(
            verwerkingsactie__vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            **default_params,
        )

        verwerkingsactie = verwerkt_object.verwerkingsactie

        VerwerktObjectFactory(
            verwerkingsactie__vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            **default_params,
        )

        url = reverse("verwerkingsactie-list")
        query_params = urlencode(default_params)
        response = self.client.get(f"{url}?{query_params}")

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerkingsactieSerializer(
            instance=verwerkingsactie,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], [serializer.data])

    def test_list_confidential_permission(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        default_params = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "123456789",
        }

        verwerkt_object_normal = VerwerktObjectFactory(
            verwerkingsactie__vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            verwerkingsactie__tijdstipRegistratie=django_timezone.now()
            - timedelta(days=1),
            **default_params,
        )

        verwerkingsactie_normal = verwerkt_object_normal.verwerkingsactie

        verwerkt_object_confidential = VerwerktObjectFactory(
            verwerkingsactie__vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            verwerkingsactie__tijdstipRegistratie=django_timezone.now(),
            **default_params,
        )

        verwerkingsactie_confidential = verwerkt_object_confidential.verwerkingsactie

        url = reverse("verwerkingsactie-list")
        query_params = urlencode(default_params)
        response = self.client.get(f"{url}?{query_params}")

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerkingsactieSerializer(
            [verwerkingsactie_confidential, verwerkingsactie_normal],
            context=dict(request=request),
            many=True,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], serializer.data)

    def test_create_vertrouwelijkheid_confidential_with_normal_permission(self):
        token = get_jwt_token(scopes=["create:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkte_object = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
        }

        response = self.client.post(
            reverse("verwerkingsactie-list"),
            {
                "actieNaam": "Actie XYZ",
                "afnemerId": "Afnemer Y",
                "bewaartermijn": "P3Y6M4DT12H30M5S",
                "handelingNaam": "Naam Y",
                "soortAfnemerId": "BSN",
                "tijdstip": tijdstip.astimezone(timezone).isoformat(),
                "uitvoerder": "10000000000000000000",
                "systeem": "Systeem X",
                "gebruiker": "Gebruiker Y",
                "gegevensbron": "Bron Z",
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
                "verwerkingId": str(verwerkingId),
                "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
                "verwerkingNaam": "Verwerking X",
                "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
                "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
                "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
                "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
                "verwerkteObjecten": [verwerkte_object],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        error = response.json()["invalidParams"]["vertrouwelijkheid"]

        self.assertEqual(
            error["reason"],
            _(
                "Aanmaken of wijzigen van Verwerkingsactie met deze vertrouwelijkheid "
                "is niet toegstaan voor de huidige gebruiker."
            ),
        )

    def test_create_vertrouwelijkheid_normal_with_confidential_permission(self):
        token = get_jwt_token(scopes=["create:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkte_object = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
        }

        response = self.client.post(
            reverse("verwerkingsactie-list"),
            {
                "actieNaam": "Actie XYZ",
                "afnemerId": "Afnemer Y",
                "bewaartermijn": "P3Y6M4DT12H30M5S",
                "handelingNaam": "Naam Y",
                "soortAfnemerId": "BSN",
                "tijdstip": tijdstip.astimezone(timezone).isoformat(),
                "uitvoerder": "10000000000000000000",
                "systeem": "Systeem X",
                "gebruiker": "Gebruiker Y",
                "gegevensbron": "Bron Z",
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
                "verwerkingId": str(verwerkingId),
                "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
                "verwerkingNaam": "Verwerking X",
                "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
                "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
                "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
                "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
                "verwerkteObjecten": [verwerkte_object],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_destroy_vertrouwelijkheid_normal_with_normal_permission(self):
        token = get_jwt_token(scopes=["delete:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactie = Verwerkingsactie.objects.create(
            tijdstip=django_timezone.now(),
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            versie=1,
        )

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingsactie.actieId),
                ),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_vertrouwelijkheid_confidential_with_normal_permission(self):
        token = get_jwt_token(scopes=["delete:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactie = Verwerkingsactie.objects.create(
            tijdstip=django_timezone.now(),
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            versie=1,
        )

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingsactie.actieId),
                ),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_destroy_vertrouwelijkheid_confidential_with_confidential_permission(self):
        token = get_jwt_token(scopes=["delete:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactie = Verwerkingsactie.objects.create(
            tijdstip=django_timezone.now(),
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            versie=1,
        )

        response = self.client.delete(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(verwerkingsactie.actieId),
                ),
            ),
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_patch_vertrouwelijkheid_with_confidential_with_normal_permission(self):
        token = get_jwt_token(scopes=["update:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        VerwerkingsactieFactory(
            verwerkingId=verwerkingId,
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_patch_vertrouwelijkheid_normal_with_confidential_permission(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        verwerkingsactie = VerwerkingsactieFactory(
            verwerkingId=verwerkingId,
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        # fmt: off
        verwerkingsactie = (
            Verwerkingsactie.objects.exclude(actieId=verwerkingsactie.actieId)
            .get()
        )
        # fmt: on

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            verwerkingsactie.vertrouwelijkheid,
            Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
        )

    def test_patch_vertrouwelijkheid_to_confidential_with_normal_permission(self):
        token = get_jwt_token(scopes=["update:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingId = str(uuid4())
        VerwerkingsactieFactory(
            verwerkingId=verwerkingId,
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
        )

        url = reverse("verwerkingsactie-list")

        response = self.client.patch(
            f"{url}?verwerkingId={str(verwerkingId)}",
            {
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        error = response.json()["invalidParams"]["vertrouwelijkheid"]

        self.assertEqual(
            error["reason"],
            _(
                "Aanmaken of wijzigen van Verwerkingsactie met deze vertrouwelijkheid "
                "is niet toegstaan voor de huidige gebruiker."
            ),
        )

    def test_put_vertrouwelijkheid_with_confidential_with_normal_permission(self):
        token = get_jwt_token(scopes=["update:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie = Verwerkingsactie.objects.create(**verwerkingsactie_data)

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
        }

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **verwerkingsactie_data,
                "systeem": "Systeem Y",
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_put_vertrouwelijkheid_normal_with_confidential_permission(self):
        token = get_jwt_token(scopes=["update:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie = Verwerkingsactie.objects.create(
            **{
                **verwerkingsactie_data,
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            },
        )

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **verwerkingsactie_data,
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_put_vertrouwelijkheid_to_confidential_with_normal_permission(self):
        token = get_jwt_token(scopes=["update:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        tijdstip = django_timezone.now() - timedelta(days=3)

        actieId = uuid4()
        verwerkingId = uuid4()
        verwerkingIdAfnemer = uuid4()
        verwerkingsactiviteitId = uuid4()
        verwerkingsactiviteitIdAfnemer = uuid4()
        verwerkingsactiviteitUrl = "https://testserver.com/activiteiten/xyz"
        verwerkingsactiviteitUrlAfnemer = "https://testserver.com/afnemers/xyz"

        verwerkingsactie_data = {
            "actieId": str(actieId),
            "actieNaam": "Actie XYZ",
            "afnemerId": "Afnemer Y",
            "bewaartermijn": "P3Y6M4DT12H30M5S",
            "handelingNaam": "Naam Y",
            "soortAfnemerId": "BSN",
            "tijdstip": tijdstip.astimezone(timezone).isoformat(),
            "uitvoerder": "10000000000000000000",
            "systeem": "Systeem X",
            "gebruiker": "Gebruiker Y",
            "gegevensbron": "Bron Z",
            "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            "verwerkingId": str(verwerkingId),
            "verwerkingIdAfnemer": str(verwerkingIdAfnemer),
            "verwerkingNaam": "Verwerking X",
            "verwerkingsactiviteitId": str(verwerkingsactiviteitId),
            "verwerkingsactiviteitIdAfnemer": str(verwerkingsactiviteitIdAfnemer),
            "verwerkingsactiviteitUrl": verwerkingsactiviteitUrl,
            "verwerkingsactiviteitUrlAfnemer": verwerkingsactiviteitUrlAfnemer,
        }

        verwerkingsactie = Verwerkingsactie.objects.create(
            **{
                **verwerkingsactie_data,
                "vertrouwelijkheid": Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            }
        )

        verwerkte_soorten_gegevens_data = {"soortGegeven": "BSN"}
        verwerkte_soorten_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
            **verwerkte_soorten_gegevens_data
        )

        verwerkte_object_data = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "1234567",
            "betrokkenheid": "Getuige",
            "verwerkteSoortenGegevens": [verwerkte_soorten_gegevens_data],
        }

        verwerktobject = VerwerktObject.objects.create(
            **{
                key: value
                for key, value in verwerkte_object_data.items()
                if key != "verwerkteSoortenGegevens"
            },
            verwerkingsactie=verwerkingsactie,
        )

        verwerktobject.verwerkteSoortenGegevens.add(verwerkte_soorten_gegeven)

        response = self.client.put(
            reverse(
                "verwerkingsactie-detail",
                kwargs=dict(
                    actieId=str(actieId),
                ),
            ),
            {
                **verwerkingsactie_data,
                "verwerkteObjecten": [verwerkte_object_data],
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_anonymous(self):
        VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )

        response = self.client.get(reverse("verwerkingsactie-list"))

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
