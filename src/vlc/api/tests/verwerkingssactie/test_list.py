from datetime import timedelta
from urllib.parse import urlencode
from uuid import uuid4

from django.conf import settings
from django.utils import timezone as django_timezone

import pytz
from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.tests.factories import VerwerktObjectFactory
from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import VerwerktObject


class VerwerkingsactieListTestCase(APITestCase):
    def test_list(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        default_params = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "123456789",
        }

        VerwerktObjectFactory.create_batch(size=3, **default_params)
        VerwerktObjectFactory(verwerkingsactie__inactive=True, **default_params)

        query_params = urlencode(default_params)
        url = reverse("verwerkingsactie-list")
        response = self.client.get(f"{url}?{query_params}")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()["results"]), 3)

    def test_list_returns_most_recent_verwerkingsacties(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        default_params = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "123456789",
        }

        actieId = uuid4()
        VerwerktObjectFactory(
            verwerkingsactie__actieId=actieId,
            verwerkingsactie__tijdstip=django_timezone.now() - timedelta(days=1),
            verwerkingsactie__versie=1,
            **default_params,
        )

        verwerkt_object = VerwerktObjectFactory(
            verwerkingsactie__actieId=actieId,
            verwerkingsactie__tijdstip=django_timezone.now(),
            verwerkingsactie__versie=2,
            **default_params,
        )

        expected_verwerkingsactie = verwerkt_object.verwerkingsactie

        query_params = urlencode(default_params)
        url = reverse("verwerkingsactie-list")
        response = self.client.get(f"{url}?{query_params}")

        timezone = pytz.timezone(settings.TIME_ZONE)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()["results"]), 1)
        self.assertEqual(
            response.json()["results"][0]["tijdstipRegistratie"],
            expected_verwerkingsactie.tijdstipRegistratie.astimezone(
                timezone
            ).isoformat(),
        )

    def test_list_inactive(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        default_params = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "123456789",
        }

        actieId = uuid4()
        VerwerktObjectFactory(
            verwerkingsactie__actieId=actieId,
            verwerkingsactie__tijdstip=django_timezone.now() - timedelta(days=1),
            verwerkingsactie__versie=1,
            **default_params,
        )

        VerwerktObjectFactory(
            verwerkingsactie__actieId=actieId,
            verwerkingsactie__tijdstip=django_timezone.now(),
            verwerkingsactie__versie=2,
            verwerkingsactie__vervallen=True,
            **default_params,
        )

        query_params = urlencode(default_params)
        url = reverse("verwerkingsactie-list")
        response = self.client.get(f"{url}?{query_params}")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()["results"]), 0)

    def test_required_query_params(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        default_params = {
            "objecttype": VerwerktObject.Objecttype.PERSOON,
            "soortObjectId": "BSN",
            "objectId": "123456789",
        }

        VerwerktObjectFactory.create_batch(size=3, **default_params)
        VerwerktObjectFactory(verwerkingsactie__inactive=True, **default_params)

        response = self.client.get(reverse("verwerkingsactie-list"))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
