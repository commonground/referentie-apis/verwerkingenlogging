from django.test.utils import override_settings
from django.urls.conf import path

from rest_framework.exceptions import APIException
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from vlc.api.views import custom404_handler, custom500_handler


def server_error_view(request):
    raise APIException


urlpatterns = [
    path("server-error/", server_error_view, name="server-error"),
]


handler404 = custom404_handler
handler500 = custom500_handler


@override_settings(ROOT_URLCONF=__name__)
class DefaultErrorViewTestCase(APITestCase):
    def setUp(self):
        self.client = self.client_class(raise_request_exception=False)

    def test_404_handler(self):
        response = self.client.get("/some/unknown/url")

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers["Content-Type"], "application/problem+json")

    def test_500_handler(self):
        response = self.client.get(reverse("server-error"))

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.headers["Content-Type"], "application/problem+json")
