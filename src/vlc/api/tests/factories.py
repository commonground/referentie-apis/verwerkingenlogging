from django.utils import timezone

import factory
import factory.fuzzy
import pytz

from vlc.datamodel.models import VerwerktObject


class VerwerkingsactieFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "datamodel.Verwerkingsactie"

    class Params:
        inactive = factory.Trait(
            tijdstipVerwijderd=timezone.now(),
            vervallen=True,
        )

    tijdstip = factory.Faker(
        "date_time_this_year", before_now=False, after_now=True, tzinfo=pytz.utc
    )
    tijdstipRegistratie = factory.Faker(
        "date_time_this_year", before_now=False, after_now=True, tzinfo=pytz.utc
    )


class VerwerktSoortGegevenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "datamodel.VerwerktSoortGegeven"
        django_get_or_create = ("soortGegeven",)

    soortGegeven = "BSN"


class VerwerktObjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "datamodel.VerwerktObject"

    verwerkingsactie = factory.SubFactory(VerwerkingsactieFactory)
    objecttype = factory.fuzzy.FuzzyChoice(
        choices=list(VerwerktObject.Objecttype.values)
    )
    soortObjectId = "BSN"

    @factory.post_generation
    def verwerkteSoortenGegevens(self, create, extracted, **kwargs):
        # optional M2M, do nothing when no arguments are passed
        if not create:
            return

        if extracted:
            for verwerkte_soort_gegeven in extracted:
                self.verwerkteSoortenGegevens.add(verwerkte_soort_gegeven)
        else:
            self.verwerkteSoortenGegevens.add(VerwerktSoortGegevenFactory())
