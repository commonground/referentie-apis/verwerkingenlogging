from django.conf import settings

from rest_framework.settings import import_from_string


class VersioningTestMixin:
    def _setup_versioning(self, request, **version_kwargs):
        """
        Setup correct attributes for requests' versioning according to
        REST_FRAMEWORK settings.
        """
        versioning_class = import_from_string(
            settings.REST_FRAMEWORK["DEFAULT_VERSIONING_CLASS"],
            "DEFAULT_VERSIONING_CLASS",
        )
        version = settings.REST_FRAMEWORK["DEFAULT_VERSION"]

        versioning_scheme = versioning_class()

        request.versioning_scheme = versioning_scheme
        request.version = versioning_scheme.determine_version(
            request, {"version": version, **version_kwargs}
        )
