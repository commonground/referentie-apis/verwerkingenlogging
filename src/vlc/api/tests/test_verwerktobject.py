from datetime import datetime, timedelta
from urllib.parse import urlencode
from uuid import uuid4

from django.conf import settings
from django.test.client import RequestFactory
from django.utils import timezone

import pytz
from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse

from vlc.api.serializers import VerwerktObjectSerializer
from vlc.api.tests.factories import VerwerkingsactieFactory, VerwerktObjectFactory
from vlc.api.tests.utils import VersioningTestMixin
from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject


class VerwerktObjectTestCase(APITestCase):
    def test_list(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        VerwerktObjectFactory.create_batch(size=3)

        response = self.client.get(reverse("verwerktobject-list"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()["results"]), 3)

    def test_retrieve(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactie = VerwerkingsactieFactory()
        verwerktobject = VerwerktObjectFactory(verwerkingsactie=verwerkingsactie)

        url = reverse(
            "verwerktobject-detail",
            kwargs=dict(
                verwerktObjectId=str(verwerktobject.verwerktObjectId),
            ),
        )
        verwerkingsactie_url = reverse(
            "verwerkingsactie-detail",
            kwargs=dict(actieId=str(verwerkingsactie.actieId)),
        )

        response = self.client.get(url)

        timezone = pytz.timezone(settings.TIME_ZONE)

        expected_verwerkingsactie = {
            "actieId": str(verwerkingsactie.actieId),
            "actieNaam": None,
            "afnemerId": None,
            "bewaartermijn": None,
            "handelingNaam": None,
            "soortAfnemerId": None,
            "tijdstip": verwerkingsactie.tijdstip.astimezone(timezone).isoformat(),
            "tijdstipRegistratie": verwerkingsactie.tijdstipRegistratie.astimezone(
                timezone
            ).isoformat(),
            "uitvoerder": None,
            "url": f"http://testserver{verwerkingsactie_url}",
            "vertrouwelijkheid": verwerkingsactie.vertrouwelijkheid.value,
            "verwerkingId": None,
            "verwerkingIdAfnemer": None,
            "verwerkingNaam": None,
            "verwerkingsactiviteitId": None,
            "verwerkingsactiviteitIdAfnemer": None,
            "verwerkingsactiviteitUrl": None,
            "verwerkingsactiviteitUrlAfnemer": None,
        }

        expected_output = {
            "url": f"http://testserver{url}",
            "verwerktObjectId": str(verwerktobject.verwerktObjectId),
            "objecttype": verwerktobject.objecttype,
            "soortObjectId": "BSN",
            "objectId": verwerktobject.objectId,
            "betrokkenheid": verwerktobject.betrokkenheid,
            "verwerkteSoortenGegevens": [{"soortGegeven": "BSN"}],
            "verwerkingsactie": expected_verwerkingsactie,
        }

        self.assertEqual(response.json(), expected_output)

    def test_method_not_allowed(self):
        token = get_jwt_token(scopes=["create:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        response = self.client.post(reverse("verwerktobject-list"), {})

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(response.headers["Content-Type"], "application/problem+json")

    def test_GET_verwerkingsobjects_when_action_is_vervallen(self):
        token = get_jwt_token(scopes=["read:confidential"])

        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(inactive=True)
        VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        response = self.client.get(reverse("verwerktobject-list"))

        self.assertEqual(len(response.json()["results"]), 0)

    def test_GET_detail_verwerkingsobjects_when_action_is_vervallen(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(inactive=True)
        verwerkingsobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        response = self.client.get(
            reverse(
                "verwerktobject-detail",
                kwargs=dict(
                    verwerktObjectId=str(verwerkingsobject.verwerktObjectId),
                ),
            )
        )
        self.assertEqual(response.json()["status"], 404)


class VerwerktObjectFilterTestCase(VersioningTestMixin, APITestCase):
    def setUp(self):
        self.base_url = reverse("verwerktobject-list")
        self.request_factory = RequestFactory()

    def test_objecttype(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        VerwerktObjectFactory(objecttype="foobar")
        VerwerktObjectFactory(objecttype="barfoo")
        expected_object = VerwerktObjectFactory(
            objecttype=VerwerktObject.Objecttype.PERSOON.value
        )

        query_params = urlencode(
            {"objecttype": VerwerktObject.Objecttype.PERSOON.value}, doseq=True
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            instance=expected_object,
            context=dict(request=request),
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], [serializer.data])

    def test_soortobject_id(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        VerwerktObjectFactory(soortObjectId="Indicator X")
        VerwerktObjectFactory(soortObjectId="Indicator Y")
        expected_object = VerwerktObjectFactory(soortObjectId="Indicator Z")

        query_params = urlencode(
            {"soortObjectId": expected_object.soortObjectId}, doseq=True
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            instance=expected_object,
            context=dict(request=request),
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], [serializer.data])

    def test_object_id(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        VerwerktObjectFactory(objectId="Object X")
        VerwerktObjectFactory(objectId="Object Y")
        expected_object = VerwerktObjectFactory(objectId="Object Z")

        query_params = urlencode({"objectId": expected_object.objectId}, doseq=True)
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            instance=expected_object,
            context=dict(request=request),
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], [serializer.data])

    def test_begindatum(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        today = timezone.now()

        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=4))
        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=5))
        expected_objects = [
            VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=2)),
            VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=3)),
        ]

        query_params = urlencode(
            {"beginDatum": (today - timedelta(days=3)).date()}, doseq=True
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            expected_objects,
            context=dict(request=request),
            many=True,
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], serializer.data)

    def test_einddatum(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        today = timezone.now()

        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=3))
        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=4))
        expected_objects = [
            VerwerktObjectFactory(
                verwerkingsactie__tijdstip=today - timedelta(days=6),
            ),
            VerwerktObjectFactory(
                verwerkingsactie__tijdstip=today - timedelta(days=7),
            ),
        ]

        query_params = urlencode(
            {"eindDatum": (today - timedelta(days=5)).date()}, doseq=True
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            expected_objects,
            context=dict(request=request),
            many=True,
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], serializer.data)

    def test_verwerkingsactiviteitId(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingsactiviteit_id = uuid4()

        VerwerktObjectFactory()
        VerwerktObjectFactory()
        expected_object = VerwerktObjectFactory(
            verwerkingsactie__verwerkingsactiviteitId=verwerkingsactiviteit_id
        )

        query_params = urlencode(
            {"verwerkingsactiviteitId": str(verwerkingsactiviteit_id)}, doseq=True
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            instance=expected_object,
            context=dict(request=request),
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], [serializer.data])

    def test_datum_combination(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        today = timezone.now()

        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=10))
        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=1))
        expected_objects = [
            VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=4)),
            VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=5)),
        ]

        query_params = urlencode(
            {
                "beginDatum": (today - timedelta(days=6)).date(),
                "eindDatum": (today - timedelta(days=2)).date(),
            },
            doseq=True,
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            expected_objects,
            context=dict(request=request),
            many=True,
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], serializer.data)

    def test_combination(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        today = timezone.now()

        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=10))
        VerwerktObjectFactory(verwerkingsactie__tijdstip=today - timedelta(days=7))
        expected_objects = [
            VerwerktObjectFactory(
                soortObjectId="Indicator Y",
                verwerkingsactie__tijdstip=today - timedelta(days=3),
            ),
            VerwerktObjectFactory(
                soortObjectId="Indicator Y",
                verwerkingsactie__tijdstip=today - timedelta(days=4),
            ),
        ]

        query_params = urlencode(
            {
                "beginDatum": (today - timedelta(days=6)).date(),
                "soortObjectId": "Indicator Y",
            },
            doseq=True,
        )
        url = f"{self.base_url}?{query_params}"

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            expected_objects,
            context=dict(request=request),
            many=True,
        )

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], serializer.data)


class VerwerktObjectPermissionTestCase(VersioningTestMixin, APITestCase):
    def setUp(self):
        self.request_factory = RequestFactory()

    def test_normal_permission_list(self):
        token = get_jwt_token(scopes=["read:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        verwerktobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )
        VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        url = reverse("verwerktobject-list")
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            instance=verwerktobject,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], [serializer.data])

    def test_normal_permission_detail(self):
        token = get_jwt_token(scopes=["read:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        verwerktobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        default_kwargs = dict(verwerktObjectId=str(verwerktobject.verwerktObjectId))
        url = reverse("verwerktobject-detail", kwargs=default_kwargs)
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request, **default_kwargs)

        serializer = VerwerktObjectSerializer(
            instance=verwerktobject,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), serializer.data)

    def test_confidential_permission_list(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL,
            tijdstip=datetime(2021, 10, 30, 12, tzinfo=pytz.utc),
        )

        verwerktobject_normal = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK,
            tijdstip=datetime(2021, 10, 30, 9, tzinfo=pytz.utc),
        )
        verwerktobject_confidential = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        url = reverse("verwerktobject-list")
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request)

        serializer = VerwerktObjectSerializer(
            [verwerktobject_normal, verwerktobject_confidential],
            context=dict(request=request),
            many=True,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["results"], serializer.data)

    def test_confidential_permission_detail(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )

        verwerktobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        default_kwargs = dict(verwerktObjectId=str(verwerktobject.verwerktObjectId))
        url = reverse("verwerktobject-detail", kwargs=default_kwargs)
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request, **default_kwargs)

        serializer = VerwerktObjectSerializer(
            instance=verwerktobject,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), serializer.data)

    def test_confidential_permission_normal_vertrouwelijkheid_detail(self):
        token = get_jwt_token(scopes=["read:confidential"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        verwerktobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        default_kwargs = dict(verwerktObjectId=str(verwerktobject.verwerktObjectId))
        url = reverse("verwerktobject-detail", kwargs=default_kwargs)
        response = self.client.get(url)

        request = self.request_factory.get(url)
        self._setup_versioning(request, **default_kwargs)

        serializer = VerwerktObjectSerializer(
            instance=verwerktobject,
            context=dict(request=request),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), serializer.data)

    def test_insufficient_permissions(self):
        token = get_jwt_token(scopes=["read:normal"])
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )

        verwerktobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        response = self.client.get(
            reverse(
                "verwerktobject-detail",
                kwargs=dict(
                    verwerktObjectId=str(verwerktobject.verwerktObjectId),
                ),
            )
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_anonymous(self):
        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.NORMAAL
        )

        verwerktobject = VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        verwerkingactie = VerwerkingsactieFactory(
            vertrouwelijkheid=Verwerkingsactie.Vertrouwelijkheid.VERTROUWELIJK
        )
        VerwerktObjectFactory(
            verwerkingsactie=verwerkingactie,
        )

        response = self.client.get(reverse("verwerktobject-list"))

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
