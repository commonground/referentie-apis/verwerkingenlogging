from rest_framework import status
from rest_framework.test import APITestCase
from vng_api_common.tests import reverse


class TokenObtainTestCase(APITestCase):
    def test_simple(self):
        response = self.client.post(
            reverse("token-obtain-pair"),
            {
                "scopes": [
                    "read:normal",
                    "create:normal",
                ]
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertCountEqual(response.json().keys(), ["refresh", "access"])

    def test_unknown_scopes(self):
        response = self.client.post(
            reverse("token-obtain-pair"),
            {
                "scopes": [
                    "read:normal",
                    "foobar:normal",
                ]
            },
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertNotIn(
            "access",
            response.json().keys(),
        )
        self.assertNotIn(
            "refresh",
            response.json().keys(),
        )

    def test_empty_scopes(self):
        response = self.client.post(
            reverse("token-obtain-pair"),
            {"scopes": []},
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertNotIn(
            "access",
            response.json().keys(),
        )
        self.assertNotIn(
            "refresh",
            response.json().keys(),
        )

    def test_without_data(self):
        response = self.client.post(reverse("token-obtain-pair"))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertNotIn(
            "access",
            response.json().keys(),
        )
        self.assertNotIn(
            "refresh",
            response.json().keys(),
        )
