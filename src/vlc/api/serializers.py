from django.utils.translation import gettext as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from vlc.api.fields import ISO8601DurationField
from vlc.api.permissions import Permission
from vlc.api.utils import get_jwt_token
from vlc.datamodel.models import Verwerkingsactie, VerwerktObject, VerwerktSoortGegeven


class VerwerktSoortGegevenSerializer(serializers.ModelSerializer):
    soortGegeven = serializers.CharField(max_length=242)

    class Meta:
        model = VerwerktSoortGegeven
        depth = 1
        fields = ["soortGegeven"]


class VerwerktObjectPartialSerializer(serializers.HyperlinkedModelSerializer):
    verwerkteSoortenGegevens = VerwerktSoortGegevenSerializer(many=True, required=False)

    class Meta:
        model = VerwerktObject
        depth = 1
        fields = [
            "objecttype",
            "soortObjectId",
            "objectId",
            "betrokkenheid",
            "verwerkteSoortenGegevens",
            "verwerktObjectId",
            "url",
        ]
        extra_kwargs = {
            "verwerktObjectId": {"read_only": True},
            "url": {"lookup_field": "verwerktObjectId"},
        }

    def create(self, validated_data):
        verwerkt_soort_gegeven_data = validated_data.pop("verwerkteSoortenGegevens", [])
        verwerkt_object = VerwerktObject.objects.create(**validated_data)

        for data in verwerkt_soort_gegeven_data:
            verwerkt_soort_gegeven, _ = VerwerktSoortGegeven.objects.get_or_create(
                soortGegeven=data["soortGegeven"]
            )
            verwerkt_object.verwerkteSoortenGegevens.add(verwerkt_soort_gegeven)

        return verwerkt_object


class VerwerkingsactieValidationMixin:
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        user_permissions = self.context.get("permissions", [])
        verwerkingactie_permissions = {
            Verwerkingsactie.Vertrouwelijkheid.get_permission(permission)
            for permission in user_permissions
        }

        if "vertrouwelijkheid" in validated_data and not any(
            (
                permission in validated_data["vertrouwelijkheid"]
                for permission in verwerkingactie_permissions
            )
        ):
            raise ValidationError(
                {
                    "vertrouwelijkheid": _(
                        "Aanmaken of wijzigen van Verwerkingsactie met deze vertrouwelijkheid "
                        "is niet toegstaan voor de huidige gebruiker."
                    )
                }
            )

        if self.instance and not any(
            (
                permission == self.instance.vertrouwelijkheid
                for permission in verwerkingactie_permissions
            )
        ):
            raise ValidationError(
                {
                    "vertrouwelijkheid": _(
                        "Wijzigen van Verwerkingsactie met deze vertrouwelijkheid "
                        "is niet toegstaan voor de huidige gebruiker."
                    )
                }
            )

        return validated_data


class VerwerkingsactiePartialSerializer(
    VerwerkingsactieValidationMixin, serializers.HyperlinkedModelSerializer
):
    bewaartermijn = ISO8601DurationField(required=False)

    class Meta:
        model = Verwerkingsactie
        depth = 1
        read_only_fields = ["url", "actieId", "tijdstipRegistratie"]
        fields = [
            "url",
            "actieId",
            "actieNaam",
            "handelingNaam",
            "verwerkingNaam",
            "verwerkingId",
            "verwerkingsactiviteitId",
            "verwerkingsactiviteitUrl",
            "vertrouwelijkheid",
            "bewaartermijn",
            "uitvoerder",
            "soortAfnemerId",
            "afnemerId",
            "verwerkingsactiviteitIdAfnemer",
            "verwerkingsactiviteitUrlAfnemer",
            "verwerkingIdAfnemer",
            "tijdstip",
            "tijdstipRegistratie",
        ]

        extra_kwargs = {"url": {"lookup_field": "actieId"}}


class VerwerkingsactieSerializer(VerwerkingsactiePartialSerializer):
    verwerkteObjecten = VerwerktObjectPartialSerializer(many=True)

    def create(self, validated_data):
        verwerkte_objecten_data = validated_data.pop("verwerkteObjecten")
        verwerkingsactie = Verwerkingsactie.objects.create(**validated_data)

        for data in verwerkte_objecten_data:
            serializer = VerwerktObjectPartialSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save(verwerkingsactie=verwerkingsactie)

        return verwerkingsactie

    def update(self, instance, validated_data):
        verwerkte_objecten_data = validated_data.pop("verwerkteObjecten")

        super().update(instance, validated_data)

        if verwerkte_objecten_data:
            instance.verwerkteObjecten.all().delete()

            for data in verwerkte_objecten_data:
                serializer = VerwerktObjectPartialSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                serializer.save(verwerkingsactie=instance)

        return instance

    class Meta(VerwerkingsactiePartialSerializer.Meta):
        fields = [
            "url",
            "actieId",
            "actieNaam",
            "handelingNaam",
            "verwerkingNaam",
            "verwerkingId",
            "verwerkingsactiviteitId",
            "verwerkingsactiviteitUrl",
            "vertrouwelijkheid",
            "bewaartermijn",
            "uitvoerder",
            "systeem",
            "gebruiker",
            "gegevensbron",
            "soortAfnemerId",
            "afnemerId",
            "verwerkingsactiviteitIdAfnemer",
            "verwerkingsactiviteitUrlAfnemer",
            "verwerkingIdAfnemer",
            "tijdstip",
            "tijdstipRegistratie",
            "verwerkteObjecten",
        ]


class VerwerkingsactiePartialUpdateSerializer(
    VerwerkingsactieValidationMixin, serializers.HyperlinkedModelSerializer
):
    bewaartermijn = ISO8601DurationField(required=False)

    class Meta:
        model = Verwerkingsactie
        depth = 1
        read_only_fields = [
            "url",
            "actieId",
            "actieNaam",
            "handelingNaam",
            "verwerkingNaam",
            "verwerkingId",
            "verwerkingsactiviteitId",
            "verwerkingsactiviteitUrl",
            "uitvoerder",
            "soortAfnemerId",
            "afnemerId",
            "verwerkingsactiviteitIdAfnemer",
            "verwerkingsactiviteitUrlAfnemer",
            "verwerkingIdAfnemer",
            "tijdstip",
            "tijdstipRegistratie",
            "verwerkteObjecten",
        ]
        fields = ["bewaartermijn", "vertrouwelijkheid"]
        extra_kwargs = {"url": {"lookup_field": "actieId"}}


class VerwerktObjectSerializer(VerwerktObjectPartialSerializer):
    verwerkingsactie = VerwerkingsactiePartialSerializer()

    class Meta(VerwerktObjectPartialSerializer.Meta):
        fields = [
            "objecttype",
            "soortObjectId",
            "objectId",
            "betrokkenheid",
            "verwerkteSoortenGegevens",
            "verwerktObjectId",
            "url",
            "verwerkingsactie",
        ]


class TokenObtainPairSerializer(serializers.Serializer):
    scopes = serializers.MultipleChoiceField(
        [permission.value.replace(".", ":") for permission in Permission],
        allow_empty=False,
    )

    def validate(self, attrs):
        super().validate(attrs)

        token = get_jwt_token(attrs["scopes"])

        return {
            "refresh": str(token),
            "access": str(token.access_token),
        }
