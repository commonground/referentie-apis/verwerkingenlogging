from isoduration import parse_duration
from rest_framework.fields import Field
from rest_framework.serializers import ValidationError


class ISO8601DurationField(Field):
    def to_internal_value(self, value):
        try:
            # verify if it's an ISO8601 duration
            parse_duration(value)
        except Exception as e:
            raise ValidationError(e)

        return value

    def to_representation(self, value):
        return value
