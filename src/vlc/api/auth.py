from enum import Enum

from rest_framework_simplejwt.models import TokenUser
from rest_framework_simplejwt.tokens import (
    AccessToken as BaseAccessToken,
    RefreshToken as BaseRefreshToken,
)

from vlc.api.permissions import PERMISSION_LEVELS

METHOD_MAPPING = {
    "GET": "read",
    "POST": "create",
    "PUT": "update",
    "PATCH": "update",
    "DELETE": "delete",
}


class TokenMixin:
    def __init__(self, token=None, verify=True):
        super().__init__(token, verify)

        if token is None:
            self.payload["permissions"] = []

    def add_perms(self, perms):
        for perm in perms:
            if perm not in self.payload["permissions"]:
                self.payload["permissions"].append(perm)

    def add_perm(self, perm):
        self.add_perms([perm])

    def has_permission(self, perm):
        return perm in self.payload["permissions"]


class AccessToken(TokenMixin, BaseAccessToken):
    """
    AccessToken structuur gebaseerd op ``simpejwt.tokens.AccessToken`` met de permissies toevoeging
    """


class RefreshToken(TokenMixin, BaseRefreshToken):
    """
    RefreshToken structuur gebaseerd op ``simplejwt.tokens.RefreshToken`` met de permissies toevoeging
    """


class User(TokenUser):
    """
    Aangepaste User class die de permissies valideert op basis van de Json Web Token
    """

    def has_perm(self, perm, obj=None):
        if isinstance(perm, Enum):
            return self.token.has_permission(perm.value)

        return self.token.has_permission(perm)

    def has_perms(self, perms, obj=None):
        if not self.is_authenticated:
            return False

        return any((self.has_perm(perm) for perm in perms))

    def get_request_permissions(self, method):
        """
        Returns the users permission(s) for a specific HTTP method
        """
        operation = METHOD_MAPPING[method]
        operation_permissions = PERMISSION_LEVELS[operation]
        user_permissions = {
            permission.split(".")[1]
            for permission in self.token.payload["permissions"]
            if permission.startswith(operation)
        }

        highest_index = max(
            [operation_permissions.index(permission) for permission in user_permissions]
        )

        return operation_permissions[: highest_index + 1]
