from django.core.management.base import BaseCommand

from vlc.api.utils import get_jwt_token


class Command(BaseCommand):
    help = "Create JWT to get access to the REST API"

    def add_arguments(self, parser):
        parser.add_argument("scopes", nargs="*", type=str)

    def handle(self, *args, scopes=None, **options):
        token = get_jwt_token(scopes=scopes)

        self.stdout.write(
            self.style.SUCCESS('token created "{}"'.format(token.access_token))
        )
