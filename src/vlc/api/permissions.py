from enum import Enum

from django.db import models

from rest_framework.permissions import BasePermission


class PermissionType(models.TextChoices):
    NORMAL = "normal"
    CONFIDENTIAL = "confidential"


# Lijst met bekende permissie-niveaus waarbij elk permissie-niveau hoger is dan de vorige
PERMISSION_LEVELS = {
    "read": [PermissionType.NORMAL.value, PermissionType.CONFIDENTIAL.value],
    "create": [PermissionType.NORMAL.value, PermissionType.CONFIDENTIAL.value],
    "update": [PermissionType.NORMAL.value, PermissionType.CONFIDENTIAL.value],
    "delete": [PermissionType.NORMAL.value, PermissionType.CONFIDENTIAL.value],
}


class Permission(Enum):
    """
    Enum met alle beschikbare permissies
    """

    READ_NORMAL = f"read.{PermissionType.NORMAL.value}"
    READ_CONFIDENTIAL = f"read.{PermissionType.CONFIDENTIAL.value}"
    CREATE_NORMAL = f"create.{PermissionType.NORMAL.value}"
    CREATE_CONFIDENTIAL = f"create.{PermissionType.CONFIDENTIAL.value}"
    UPDATE_NORMAL = f"update.{PermissionType.NORMAL.value}"
    UPDATE_CONFIDENTIAL = f"update.{PermissionType.CONFIDENTIAL.value}"
    DESTROY_NORMAL = f"delete.{PermissionType.NORMAL.value}"
    DESTROY_CONFIDENTIAL = f"delete.{PermissionType.CONFIDENTIAL.value}"


class APIPermissions(BasePermission):
    """
    Aangepaste Django Rest Framework permission class die de vereiste permissies uit de viewset valideert bij de
    gebruiker
    """

    def has_permission(self, request, view):
        """
        Geeft terug of de gebruiker de vereiste rechten heeft
        """

        if not hasattr(request, "user") or not request.user.is_authenticated:
            return False

        # Empty permission list means no permissions and all users are allowed.
        # No view action means the view cannot determine the action and will return
        # an MethodNotAllowedResponse.
        if not hasattr(view, "permissions") or not view.action:
            return True

        permissions = view.permissions.get(request.method, [])
        return request.user.has_perms(permissions)

    def has_object_permission(self, request, view, obj):
        """
        Geeft terug of de gebruiker de vereiste rechten heeft voor een specifiek object
        """
        from vlc.datamodel.models import Verwerkingsactie

        if not self.has_permission(request, view):
            return False

        object_permission = self.get_object_permission(obj)
        user_permissions = request.user.get_request_permissions(request.method)

        user_object_permissions = {
            Verwerkingsactie.Vertrouwelijkheid.get_permission(permission).value
            for permission in user_permissions
        }

        return object_permission in user_object_permissions

    def get_object_permission(self, obj):
        raise NotImplemented


class VerwerkingsactiePermissions(APIPermissions):
    def get_object_permission(self, obj):
        return obj.vertrouwelijkheid


class VerwerktObjectPermissions(APIPermissions):
    def get_object_permission(self, obj):
        return obj.verwerkingsactie.vertrouwelijkheid
