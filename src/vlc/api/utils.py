import logging

from vlc.api.auth import RefreshToken
from vlc.api.permissions import PERMISSION_LEVELS, Permission

logger = logging.getLogger(__name__)


class FakeUser:
    id = 1


def get_jwt_token(scopes=["read:normal"]):
    token = RefreshToken.for_user(FakeUser())
    permission_names = [member.value for member in list(Permission)]

    if scopes is not None and len(scopes) > 0:
        for scope in scopes:
            if ":" not in scope:
                logger.warning(f'ignoring invalid scope "{scope}"')
                continue

            (operation, level) = scope.split(":")

            permission_levels = PERMISSION_LEVELS.get(operation)
            if permission_levels is None:
                logger.warning(f'ignoring invalid operation "{operation}"')
                continue

            if level not in permission_levels:
                logger.warning(
                    f'ignoring invalid level "{level}" for operation "{operation}"'
                )
                continue

            # Converts a scope (format: 'OPERATION_NAME:MAX_LEVEL') to a list of permissions
            additional_permission_levels = permission_levels[
                : permission_levels.index(level) + 1
            ]
            perms = [".".join([operation, lvl]) for lvl in additional_permission_levels]

            for perm in perms:
                if perm not in permission_names:
                    logger.warning(f'ignoring invalid scope "{scope}"')
                    continue

                token.add_perm(perm)

        return token
