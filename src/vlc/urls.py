from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.views.generic.base import TemplateView

from vng_api_common.views import ViewConfigView

from vlc.api.views import custom404_handler, custom500_handler

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("vlc.api.urls")),
    path("", TemplateView.as_view(template_name="index.html"), name="index"),
    path("view-config/", ViewConfigView.as_view(), name="view-config"),
    path("ref/", include("vng_api_common.urls")),
]

urlpatterns += staticfiles_urlpatterns() + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

handler404 = custom404_handler
handler500 = custom500_handler
