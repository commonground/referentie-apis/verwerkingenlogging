from datetime import timedelta

from vng_api_common.conf.api import *  # noqa - imports white-listed

from .environ import config  # noqa

API_VERSION = "0.9.0-alpha2"

REST_FRAMEWORK = BASE_REST_FRAMEWORK.copy()
REST_FRAMEWORK.update(
    {
        # TODO: we should use the CamelCaseJSONRenderer/CamelCaseJSONParser.
        # It will require renaming (and migrating) alot of fields though.
        "DEFAULT_RENDERER_CLASSES": [
            "rest_framework.renderers.JSONRenderer",
            "rest_framework.renderers.BrowsableAPIRenderer",
        ],
        "DEFAULT_PARSER_CLASSES": [
            "rest_framework.parsers.JSONParser",
            "rest_framework.parsers.FormParser",
            "rest_framework.parsers.MultiPartParser",
        ],
        "PAGE_SIZE": 100,
        "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
        "DEFAULT_AUTHENTICATION_CLASSES": (
            ("rest_framework_simplejwt.authentication.JWTTokenUserAuthentication",)
        ),
        "DEFAULT_FILTER_BACKENDS": [
            "django_filters.rest_framework.DjangoFilterBackend"
        ],
        "DATETIME_INPUT_FORMATS": ["iso-8601"],
        "EXCEPTION_HANDLER": "vlc.api.exceptions.handler",  # TODO: deprecate this
    }
)

SECURITY_DEFINITION_NAME = "JWT-Claims"

SWAGGER_SETTINGS = BASE_SWAGGER_SETTINGS.copy()
SWAGGER_SETTINGS.update(
    {
        "DEFAULT_INFO": "vlc.api.schema.info",
        "SECURITY_DEFINITIONS": {
            SECURITY_DEFINITION_NAME: {
                "type": "http",
                "scheme": "bearer",
                "bearerFormat": "JWT",
            }
        },
    }
)

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(days=1000),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=1000),
    "ROTATE_REFRESH_TOKENS": False,
    "BLACKLIST_AFTER_ROTATION": True,
    "UPDATE_LAST_LOGIN": False,
    "ALGORITHM": "HS256",
    "VERIFYING_KEY": None,
    "AUDIENCE": None,
    "ISSUER": "vlc-referentie-implementatie",
    "AUTH_HEADER_TYPES": ("Bearer",),
    "AUTH_HEADER_NAME": "HTTP_AUTHORIZATION",
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "sub",
    "AUTH_TOKEN_CLASSES": ("vlc.api.auth.AccessToken",),
    "TOKEN_TYPE_CLAIM": "token_type",
    "TOKEN_USER_CLASS": "vlc.api.auth.User",
    "JTI_CLAIM": "jti",
}


GEMMA_URL_INFORMATIEMODEL = "Imztc"
GEMMA_URL_INFORMATIEMODEL_VERSIE = "2.1"

repo = "VNG-Realisatie/VNG-referentielijsten"
commit = "da1b2cfdaadb2d19a7d3fc14530923913a2560f2"
REFERENTIELIJSTEN_API_SPEC = (
    f"https://raw.githubusercontent.com/{repo}/{commit}/src/openapi.yaml"  # noqa
)

SELF_REPO = "commonground/referentie-apis/verwerkingenlogging"
SELF_BRANCH = config("SELF_BRANCH", default="") or API_VERSION
GITLAB_API_SPEC = (
    f"https://gitlab.com/{SELF_REPO}/-/tree/{SELF_BRANCH}/src/openapi.yaml"  # noqa
)
GITHUB_API_SPEC = None

SCOPES_SPEC = "https://github.com/VNG-Realisatie/gemma-verwerkingenlogging/blob/master/docs/api-write/oas-specification/logging-verwerkingen-api/scopes.md"

POSTMAN_COLLECTION_URL = "https://documenter.getpostman.com/view/6277499/UVsEUoJQ"
DOCUMENTATION_URL = "https://vng-realisatie.github.io/gemma-zaken"

SPEC_CACHE_TIMEOUT = 60 * 60 * 24  # 24 hours
