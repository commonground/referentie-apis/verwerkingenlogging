import sys

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
ALLOWED_HOSTS = ["localhost", "127.0.0.1", "testserver.com"]

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-2%)qv_*6c!1xb$75*2d-_ji@7^im&@fmx5+!^j8t)ec3&24x58"
SIMPLE_JWT["SIGNING_KEY"] = SECRET_KEY

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

LOGGING["loggers"].update(
    {
        "vlc": {
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "django": {"handlers": ["console"], "level": "INFO", "propagate": True},
        "django.db.backends": {
            "handlers": ["django"],
            "level": "DEBUG",
            "propagate": False,
        },
        "performance": {"handlers": ["console"], "level": "INFO", "propagate": True},
    }
)

if "test" not in sys.argv:
    REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"] += (
        "rest_framework.renderers.BrowsableAPIRenderer",
    )

# Override settings with local settings.
try:
    from .local import *  # noqa
except ImportError:
    pass
