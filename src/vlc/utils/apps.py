from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = "vlc.utils"

    def read(self):
        from . import checks  # noqa
