# Resources

Dit document beschrijft de (RGBZ-)objecttypen die als resources ontsloten
worden met de beschikbare attributen.


## VerwerktSoortGegeven

Objecttype op [GEMMA Online](https://www.gemmaonline.nl/index.php/Imztc_2.1/doc/objecttype/verwerktsoortgegeven)

| Attribuut | Omschrijving | Type | Verplicht | CRUD* |
| --- | --- | --- | --- | --- |
| soortGegeven |  | string | ja | C​R​U​D |

## Verwerkingsactie

Objecttype op [GEMMA Online](https://www.gemmaonline.nl/index.php/Imztc_2.1/doc/objecttype/verwerkingsactie)

| Attribuut | Omschrijving | Type | Verplicht | CRUD* |
| --- | --- | --- | --- | --- |
| url | URL-referentie naar dit object. Dit is de unieke identificatie en locatie van dit object. | string | nee | ~~C~~​R​~~U~~​~~D~~ |
| actieId |  | string | nee | ~~C~~​R​~~U~~​~~D~~ |
| actieNaam |  | string | nee | C​R​U​D |
| handelingNaam |  | string | nee | C​R​U​D |
| verwerkingNaam |  | string | nee | C​R​U​D |
| verwerkingId |  | string | nee | C​R​U​D |
| verwerkingsactiviteitId | Het ID van het verwerkingsactiviteit. | string | nee | C​R​U​D |
| verwerkingsactiviteitUrl |  | string | nee | C​R​U​D |
| vertrouwelijkheid |  | string | nee | C​R​U​D |
| bewaartermijn |  | string | nee | C​R​U​D |
| uitvoerder |  | string | nee | C​R​U​D |
| systeem |  | string | nee | C​R​U​D |
| gebruiker |  | string | nee | C​R​U​D |
| gegevensbron |  | string | nee | C​R​U​D |
| soortAfnemerId |  | string | nee | C​R​U​D |
| afnemerId |  | string | nee | C​R​U​D |
| verwerkingsactiviteitIdAfnemer |  | string | nee | C​R​U​D |
| verwerkingsactiviteitUrlAfnemer |  | string | nee | C​R​U​D |
| verwerkingIdAfnemer |  | string | nee | C​R​U​D |
| tijdstip |  | string | ja | C​R​U​D |
| tijdstipRegistratie |  | string | nee | ~~C~~​R​~~U~~​~~D~~ |
| verwerkteObjecten |  | array | ja | C​R​U​D |

## VerwerktObject

Objecttype op [GEMMA Online](https://www.gemmaonline.nl/index.php/Imztc_2.1/doc/objecttype/verwerktobject)

| Attribuut | Omschrijving | Type | Verplicht | CRUD* |
| --- | --- | --- | --- | --- |
| objecttype | Soort object dat verwerkt is. | string | ja | C​R​U​D |
| soortObjectId | Soort identificator waarmee het object geïdentificeerd wordt. | string | ja | C​R​U​D |
| objectId | Identificator van het verwerkte object. | string | nee | C​R​U​D |
| betrokkenheid | Geeft aan in welke hoedanigheid de persoon of het object betrokken was bij de verwerkingsactie. | string | nee | C​R​U​D |
| verwerkteSoortenGegevens |  | array | nee | C​R​U​D |
| verwerktObjectId | Identificator van het verwerkte object. Deze ID wordt automatisch aangemaakt door het log. | string | nee | ~~C~~​R​~~U~~​~~D~~ |
| url | URL-referentie naar dit object. Dit is de unieke identificatie en locatie van dit object. | string | nee | ~~C~~​R​~~U~~​~~D~~ |


* Create, Read, Update, Delete
